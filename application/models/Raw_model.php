<?php
class Raw_model extends CI_Model {
    //this function below to manage /
        //$ci = &get_instance();
    public function __construct()
    {
      parent::__construct();
    }


    public function add_data($data){
      $this->db->insert('raw_material',$data);
      return true;
    }

    public function selectAll($dataToSearch=null ,$per_page=null, $from=null ,$is_paginate=false){
      if($is_paginate)
      {
        $q = $this->db->limit($per_page,$from);
      }
      $q =  $this->db->select('*')->from('raw_material');
      if(isset($dataToSearch['rm_name']) && !empty($dataToSearch['rm_name']))
      {
        $q =  $this->db->like('rm_name',$dataToSearch['rm_name']);
      } 
      if(isset($dataToSearch['rm_code']) && !empty($dataToSearch['rm_code']))
      {
        $q =  $this->db->like('rm_code',$dataToSearch['rm_code']);
      }  
      $q = $this->db->where('raw_material.is_active',true)->get()->result();
      return $q;
    }

    public function count_selectAll($dataToSearch=null)
    {
      $q= $this->db->select('COUNT(raw_material.rm_id) as total')->from('raw_material');
      if(isset($dataToSearch['rm_name']) && !empty($dataToSearch['rm_name']))
      {
        $q =  $this->db->like('rm_name',$dataToSearch['rm_name']);
      } 
      if(isset($dataToSearch['rm_code']) && !empty($dataToSearch['rm_code']))
      {
        $q =  $this->db->like('rm_code',$dataToSearch['rm_code']);
      }       
      $q = $this->db->where('raw_material.is_active',true)->get()->row();
      return $q->total;
    }

    public function getDetail($dataToSearch=null){
      $q =  $this->db->select('*')->from('raw_material');
      if(isset($dataToSearch['rm_name']) && !empty($dataToSearch['rm_name']))
      {
        $q =  $this->db->like('rm_name',$dataToSearch['rm_name']);
      } 
      if(isset($dataToSearch['rm_code']) && !empty($dataToSearch['rm_code']))
      {
        $q =  $this->db->like('rm_code',$dataToSearch['rm_code']);
      }  
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('rm_id',$dataToSearch['rm_id']);
      }  
      $q = $this->db->where('raw_material.is_active',true)->get()->row();
      return $q;
      
    }

    public function saveEdit($data_to_update, $data_where)
    {
      $this->db->set($data_to_update)->where($data_where)->update('raw_material');
      return true;
    }
}
