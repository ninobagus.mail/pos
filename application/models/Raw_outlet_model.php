<?php
class Raw_outlet_model extends CI_Model {
    //this function below to manage /
        //$ci = &get_instance();
    public function __construct()
    {
      parent::__construct();
    }


    public function add_data($data){
      $this->db->insert('raw_material_outlet',$data);
      return true;
    }

    public function selectAll($dataToSearch=null ,$per_page=null, $from=null ,$is_paginate=false){
      if($is_paginate)
      {
        $q = $this->db->limit($per_page,$from);
      }
      $q =  $this->db->select('raw_material_outlet.*, outlets.name as outlet_name , raw_material.rm_name ,raw_material.rm_unit')->from('raw_material_outlet')
      ->join('outlets','outlets.id =  raw_material_outlet.outlet_id','left')
      ->join('raw_material','raw_material.rm_id =  raw_material_outlet.rm_id','left')
      ;
      if(isset($dataToSearch['outlet_id']) && !empty($dataToSearch['outlet_id']))
      {
        $q =  $this->db->where('outlet_id',$dataToSearch['outlet_id']);
      } 
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('rm_id',$dataToSearch['rm_id']);
      } 
      if(isset($dataToSearch['rmo_id']) && !empty($dataToSearch['rmo_id']))
      {
        $q =  $this->db->where('rmo_id',$dataToSearch['rmo_id']);
      } 

      $q = $this->db->where('raw_material_outlet.is_active',true)->get()->result();
      return $q;
    }

    public function count_selectAll($dataToSearch=null)
    {
      $q= $this->db->select('COUNT(raw_material_outlet.rmo_id) as total')->from('raw_material_outlet')
       ->join('outlets','outlets.id =  raw_material_outlet.outlet_id','left')
      ->join('raw_material','raw_material.rm_id =  raw_material_outlet.rm_id','left');
      if(isset($dataToSearch['outlet_id']) && !empty($dataToSearch['outlet_id']))
      {
        $q =  $this->db->where('outlet_id',$dataToSearch['outlet_id']);
      } 
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('rm_id',$dataToSearch['rm_id']);
      } 
      if(isset($dataToSearch['supplier_id']) && !empty($dataToSearch['supplier_id']))
      {
        $q =  $this->db->where('supplier_id',$dataToSearch['supplier_id']);
      } 
      if(isset($dataToSearch['rmo_id']) && !empty($dataToSearch['rmo_id']))
      {
        $q =  $this->db->where('rmo_id',$dataToSearch['rmo_id']);
      }    
      if(isset($dataToSearch['startDate']) && !empty($dataToSearch['startDate']))
      {
        $q =  $this->db->where('trx_date >=',$dataToSearch['startDate']);
      } 
      if(isset($dataToSearch['endDate']) && !empty($dataToSearch['endDate']))
      {
        $q =  $this->db->where('trx_date <=',$dataToSearch['endDate']);
      } 
      $q = $this->db->where('raw_material_outlet.is_active',true)->get()->row();
      return $q->total;
    }

    public function getDetail($dataToSearch=null){
      $q =  $this->db->select('raw_material_outlet.*, outlets.name as outlet_name , raw_material.rm_name ,raw_material.rm_unit')->from('raw_material_outlet')
      ->join('outlets','outlets.id =  raw_material_outlet.outlet_id','left')
      ->join('raw_material','raw_material.rm_id =  raw_material_outlet.rm_id','left')
      ;
      if(isset($dataToSearch['outlet_id']) && !empty($dataToSearch['outlet_id']))
      {
        $q =  $this->db->where('outlet_id',$dataToSearch['outlet_id']);
      } 
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('rm_id',$dataToSearch['rm_id']);
      } 
      if(isset($dataToSearch['rmo_id']) && !empty($dataToSearch['rmo_id']))
      {
        $q =  $this->db->where('rmo_id',$dataToSearch['rmo_id']);
      } 
      $q = $this->db->where('raw_material_outlet.is_active',true)->get()->row();
      return $q;
      
    }

    public function saveEdit($data_to_update, $data_where)
    {
      $this->db->set($data_to_update)->where($data_where)->update('raw_material_outlet');
      return true;
    }
}
