<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function record_category_count()
    {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('category');
        $this->db->save_queries = false;

        return $query->num_rows();
    }

    public function fetch_category_data($limit, $start)
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get('category');

        $result = $query->result();

        $this->db->save_queries = false;

        return $result;
    }

    public function record_product_count()
    {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('products');
        $this->db->save_queries = false;

        return $query->num_rows();
    }

    public function fetch_product_data($limit, $start)
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get('products');

        $result = $query->result();

        $this->db->save_queries = false;

        return $result;
    }

    public function record_label_count()
    {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('products');
        $this->db->save_queries = false;

        return $query->num_rows();
    }

    public function fetch_label_data($limit, $start)
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get('products');

        $result = $query->result();

        $this->db->save_queries = false;

        return $result;
    }

    public function getDetail($dataToSearch=null){
      $q =  $this->db->select('*')->from('products');
      if(isset($dataToSearch['id']) && !empty($dataToSearch['id']))
      {
        $q =  $this->db->like('id',$dataToSearch['id']);
      } 
      if(isset($dataToSearch['code']) && !empty($dataToSearch['code']))
      {
        $q =  $this->db->like('code',$dataToSearch['code']);
      } 
      if(isset($dataToSearch['name']) && !empty($dataToSearch['name']))
      {
        $q =  $this->db->like('name',$dataToSearch['name']);
      }  
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('rm_id',$dataToSearch['rm_id']);
      }  
      $q = $this->db->where('products.status',true)->get()->row();
      return $q;
      
    }
}
