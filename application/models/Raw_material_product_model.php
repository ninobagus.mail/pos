<?php
class Raw_material_product_model extends CI_Model {
    //this function below to manage /
        //$ci = &get_instance();
    public function __construct()
    {
      parent::__construct();
    }


    public function add($data){
      $this->db->insert('raw_material_product',$data);
      return true;
    }

    public function selectAll($dataToSearch=null ,$per_page=null, $from=null ,$is_paginate=false){
      if($is_paginate)
      {
        $q = $this->db->limit($per_page,$from);
      }
      $q =  $this->db->select('raw_material_product.* , raw_material.rm_name, raw_material.rm_unit')->from('raw_material_product')
      ->join('raw_material','raw_material.rm_id = raw_material_product.rm_id','left');
      if(isset($dataToSearch['product_id']) && !empty($dataToSearch['product_id']))
      {
        $q =  $this->db->where('product_id',$dataToSearch['product_id']);
      }    
      $q = $this->db->where('raw_material_product.is_active',true)->get()->result();
      return $q;
    }

    public function getDetail($dataToSearch=null){
      $q =  $this->db->select('raw_material_product.* , raw_material.rm_name, raw_material.rm_unit')->from('raw_material_product')
      ->join('raw_material','raw_material.rm_id = raw_material_product.rm_id','left');
      if(isset($dataToSearch['product_id']) && !empty($dataToSearch['product_id']))
      {
        $q =  $this->db->where('product_id',$dataToSearch['product_id']);
      }
      $q = $this->db->where('raw_material_product.is_active',true)->get()->row();
      return $q;
      
    }

    public function saveEdit($data_to_update, $data_where)
    {
      $this->db->set($data_to_update)->where($data_where)->update('raw_material_product');
      return true;
    }
}
