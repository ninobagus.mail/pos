<?php
class Raw_return_model extends CI_Model {
    //this function below to manage /
        //$ci = &get_instance();
    public function __construct()
    {
      parent::__construct();
    }


    public function saveTrx($trxHeader , $trxDetail){
      
      $this->db->insert('return_raw_header',$trxHeader);
      $this->db->insert('return_raw_detail',$trxDetail);
    }

    public function selectAll($dataToSearch=null ,$per_page=null, $from=null ,$is_paginate=false){
      if($is_paginate)
      {
        $q = $this->db->limit($per_page,$from);
      }
      $q =  $this->db->select('return_raw_header.* , return_raw_detail.rm_id , return_raw_detail.qty , return_raw_detail.price_per_qty , return_raw_detail.reason , raw_material.rm_name , raw_material.rm_unit , outlets.name , suppliers.name as supplier_name')->from('return_raw_header')
      ->join('return_raw_detail','return_raw_detail.trx_no =  return_raw_header.trx_no','left')
      ->join('raw_material','raw_material.rm_id =  return_raw_detail.rm_id','left')
      ->join('outlets','outlets.id =  return_raw_header.outlet_id','left')
      ->join('suppliers','suppliers.id =  return_raw_detail.supplier_id','left')
      ;
      if(isset($dataToSearch['trx_no']) && !empty($dataToSearch['trx_no']))
      {
        $q =  $this->db->like('return_raw_header.trx_no',$dataToSearch['trx_no']);
      } 
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('return_raw_detail.rm_id',$dataToSearch['rm_id']);
      } 
      if(isset($dataToSearch['payment_method_id']) && !empty($dataToSearch['payment_method_id']))
      {
        $q =  $this->db->where('return_raw_header.payment_method_id',$dataToSearch['payment_method_id']);
      } 
      if(isset($dataToSearch['outlet_id']) && !empty($dataToSearch['outlet_id']))
      {
        $q =  $this->db->where('return_raw_header.outlet_id',$dataToSearch['outlet_id']);
      } 
      if(isset($dataToSearch['start_date']) && !empty($dataToSearch['start_date']))
      {
        $q =  $this->db->where('trx_date >=',$dataToSearch['start_date']);
      } 
      if(isset($dataToSearch['end_date']) && !empty($dataToSearch['end_date']))
      {
       $q =  $this->db->where('trx_date <=',$dataToSearch['end_date']);
      } 
      $q = $this->db->get()->result();
      return $q;
    }

    public function count_selectAll($dataToSearch=null)
    {
      $q= $this->db->select('COUNT(return_raw_header.trx_id) as total')->from('return_raw_header')
       ->join('return_raw_detail','return_raw_detail.trx_no =  return_raw_header.trx_no','left')
      ->join('raw_material','raw_material.rm_id =  return_raw_detail.rm_id','left')
      ->join('outlets','outlets.id =  return_raw_header.outlet_id','left')
      ->join('suppliers','suppliers.id =  return_raw_detail.supplier_id','left')
      ;
      if(isset($dataToSearch['trx_no']) && !empty($dataToSearch['trx_no']))
      {
        $q =  $this->db->like('return_raw_header.trx_no',$dataToSearch['trx_no']);
      } 
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('return_raw_detail.rm_id',$dataToSearch['rm_id']);
      } 
      if(isset($dataToSearch['payment_method_id']) && !empty($dataToSearch['payment_method_id']))
      {
        $q =  $this->db->where('return_raw_header.payment_method_id',$dataToSearch['payment_method_id']);
      } 
      if(isset($dataToSearch['outlet_id']) && !empty($dataToSearch['outlet_id']))
      {
        $q =  $this->db->where('return_raw_header.outlet_id',$dataToSearch['outlet_id']);
      } 
      if(isset($dataToSearch['start_date']) && !empty($dataToSearch['start_date']))
      {
        $q =  $this->db->where('trx_date >=',$dataToSearch['start_date']);
      } 
      if(isset($dataToSearch['end_date']) && !empty($dataToSearch['end_date']))
      {
       $q =  $this->db->where('trx_date <=',$dataToSearch['end_date']);
      } 
      $q = $this->db->get()->row();
      return $q->total;
    }

    public function getDetail($dataToSearch=null){
     $q =  $this->db->select('return_raw_header.* , return_raw_detail.rm_id , return_raw_detail.qty , return_raw_detail.price_per_qty , return_raw_detail.reason , raw_material.rm_name , raw_material.rm_unit , outlets.name , suppliers.name')->from('return_raw_header')
      ->join('return_raw_detail','return_raw_detail.trx_no =  return_raw_header.trx_no','left')
      ->join('raw_material','raw_material.rm_id =  return_raw_detail.rm_id','left')
      ->join('outlets','outlets.id =  return_raw_header.outlet_id','left')
      ->join('suppliers','suppliers.id =  return_raw_detail.supplier_id','left')
      ;
      if(isset($dataToSearch['trx_no']) && !empty($dataToSearch['trx_no']))
      {
        $q =  $this->db->like('return_raw_header.trx_no',$dataToSearch['trx_no']);
      } 
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('return_raw_detail.rm_id',$dataToSearch['rm_id']);
      } 
      if(isset($dataToSearch['payment_method_id']) && !empty($dataToSearch['payment_method_id']))
      {
        $q =  $this->db->where('return_raw_header.payment_method_id',$dataToSearch['payment_method_id']);
      } 
      if(isset($dataToSearch['outlet_id']) && !empty($dataToSearch['outlet_id']))
      {
        $q =  $this->db->where('return_raw_header.outlet_id',$dataToSearch['outlet_id']);
      } 
      if(isset($dataToSearch['start_date']) && !empty($dataToSearch['start_date']))
      {
        $q =  $this->db->where('trx_date >=',$dataToSearch['start_date']);
      } 
      if(isset($dataToSearch['end_date']) && !empty($dataToSearch['end_date']))
      {
       $q =  $this->db->where('trx_date <=',$dataToSearch['end_date']);
      } 
      $q = $this->db->get()->row();
      return $q;
      
    }

    public function saveEdit($data_to_update, $data_where)
    {

    }
}
