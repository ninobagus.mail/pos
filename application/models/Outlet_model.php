<?php
class Outlet_model extends CI_Model {
    //this function below to manage /
        //$ci = &get_instance();
    public function __construct()
    {
      parent::__construct();
    }

    public function selectAll($dataToSearch=null ,$per_page=null, $from=null ,$is_paginate=false){
      if($is_paginate)
      {
        $q = $this->db->limit($per_page,$from);
      }
      $q =  $this->db->select('*')->from('outlets')->get()->result();
      return $q;
    }

    public function selectAllForKasir()
    {
       $q =  $this->db->select('outlets.*')->from('outlets')->join('users','users.outlet_id =  outlets.id','left')->where('users.role_id',$this->session->userdata('user_role'))->where('users.id',$this->session->userdata('user_id'))->get()->result();
       return $q;
    }

}
