<?php
class Raw_material_outlet_model extends CI_Model {
    //this function below to manage /
        //$ci = &get_instance();
    public function __construct()
    {
      parent::__construct();
    }


    public function add($data){
      $this->db->insert('raw_material_outlet',$data);
      return true;
    }

    public function selectAll($dataToSearch=null ,$per_page=null, $from=null ,$is_paginate=false){
      if($is_paginate)
      {
        $q = $this->db->limit($per_page,$from);
      }
      $q =  $this->db->select('raw_material_outlet.*, outlets.name as outlet_name , raw_material.rm_name ,raw_material.rm_unit')->from('raw_material_outlet')
      ->join('outlets','outlets.id =  raw_material_outlet.outlet_id','left')
      ->join('raw_material','raw_material.rm_id =  raw_material_outlet.rm_id','left')
      ;
      if(isset($dataToSearch['outlet_id']) && !empty($dataToSearch['outlet_id']))
      {
        $q =  $this->db->where('outlet_id',$dataToSearch['outlet_id']);
      } 
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('rm_id',$dataToSearch['rm_id']);
      } 
      if(isset($dataToSearch['rmo_id']) && !empty($dataToSearch['rmo_id']))
      {
        $q =  $this->db->where('rmo_id',$dataToSearch['rmo_id']);
      } 

      $q = $this->db->where('raw_material_outlet.is_active',true)->get()->result();
      return $q;
    }

    public function selectAllHistory($dataToSearch=null)
    {
       $q =  $this->db->select('SUM(rmo_history.total_stock) as total_stock,SUM(rmo_history.used_stock) as used_stock,rmo_history.i_date, outlets.name as outlet_name , raw_material.rm_name ,raw_material.rm_unit ,outlets.name')->from('rmo_history')
      ->join('outlets','outlets.id =  rmo_history.outlet_id','left')
      ->join('raw_material','raw_material.rm_id =  rmo_history.rm_id','left')
      ;
      if(isset($dataToSearch['outlet_id']) && !empty($dataToSearch['outlet_id']))
      {
        $q =  $this->db->where('outlet_id',$dataToSearch['outlet_id']);
      } 
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('rm_id',$dataToSearch['rm_id']);
      } 
      if(isset($dataToSearch['start_date']) && !empty($dataToSearch['start_date']))
      {
        $q =  $this->db->where('i_date >=',$dataToSearch['start_date']);
      } 
      if(isset($dataToSearch['end_date']) && !empty($dataToSearch['end_date']))
      {
        $q =  $this->db->where('i_date <=',$dataToSearch['end_date']);
      } 

      $q = $this->db->group_by('i_date')->group_by('rmo_history.rm_id')->get()->result();
      return $q;
    }

    public function count_selectAll($dataToSearch=null)
    {
      $q= $this->db->select('COUNT(raw_material_outlet.rmo_id) as total')->from('raw_material_outlet')
       ->join('outlets','outlets.id =  raw_material_outlet.outlet_id','left')
      ->join('raw_material','raw_material.rm_id =  raw_material_outlet.rm_id','left');
      if(isset($dataToSearch['outlet_id']) && !empty($dataToSearch['outlet_id']))
      {
        $q =  $this->db->where('outlet_id',$dataToSearch['outlet_id']);
      } 
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('rm_id',$dataToSearch['rm_id']);
      } 
      if(isset($dataToSearch['supplier_id']) && !empty($dataToSearch['supplier_id']))
      {
        $q =  $this->db->where('supplier_id',$dataToSearch['supplier_id']);
      } 
      if(isset($dataToSearch['rmo_id']) && !empty($dataToSearch['rmo_id']))
      {
        $q =  $this->db->where('rmo_id',$dataToSearch['rmo_id']);
      }    
      if(isset($dataToSearch['startDate']) && !empty($dataToSearch['startDate']))
      {
        $q =  $this->db->where('trx_date >=',$dataToSearch['startDate']);
      } 
      if(isset($dataToSearch['endDate']) && !empty($dataToSearch['endDate']))
      {
        $q =  $this->db->where('trx_date <=',$dataToSearch['endDate']);
      } 
      $q = $this->db->where('raw_material_outlet.is_active',true)->get()->row();
      return $q->total;
    }

    public function getDetail($dataToSearch=null){
      $q =  $this->db->select('raw_material_outlet.*, outlets.name as outlet_name , raw_material.rm_name ,raw_material.rm_unit')->from('raw_material_outlet')
      ->join('outlets','outlets.id =  raw_material_outlet.outlet_id','left')
      ->join('raw_material','raw_material.rm_id =  raw_material_outlet.rm_id','left')
      ;
      if(isset($dataToSearch['outlet_id']) && !empty($dataToSearch['outlet_id']))
      {
        $q =  $this->db->where('outlet_id',$dataToSearch['outlet_id']);
      } 
      if(isset($dataToSearch['rm_id']) && !empty($dataToSearch['rm_id']))
      {
        $q =  $this->db->where('rm_id',$dataToSearch['rm_id']);
      } 
      if(isset($dataToSearch['rmo_id']) && !empty($dataToSearch['rmo_id']))
      {
        $q =  $this->db->where('rmo_id',$dataToSearch['rmo_id']);
      } 
      $q = $this->db->where('raw_material_outlet.is_active',true)->get()->row();
      return $q;
      
    }

    public function saveEdit($data_to_update, $data_where)
    {
      $this->db->set($data_to_update)->where($data_where)->update('raw_material_outlet');
      return true;
    }

    public function insertIntoRMOHistory($rm_id , $outlet_id , $type ,$qty, $old_qty=0)
    {
      $date= date('Y-m-d H:i:s');
      $i_date =  date('Y-m-d');
      if($type=='add_from_supplier')
      {
        $add= array(
          'rm_id' =>$rm_id,
          'outlet_id' =>$outlet_id,
          'used_stock'=>0,
          'total_stock'=>$qty,
          'create_by'=>$this->session->userdata('user_id'),
          'create_date'=>date('Y-m-d H:i:s'),
          'i_date'  => $i_date
        );
        $this->db->insert('rmo_history',$add);

      }
      else if($type=='update_from_supplier')
      {
        $add= array(
          'rm_id' =>$rm_id,
          'outlet_id' =>$outlet_id,
          'used_stock'=>0,
          'total_stock'=>$qty,
          'create_by'=>$this->session->userdata('user_id'),
          'create_date'=>date('Y-m-d H:i:s'),
          'i_date'  => $i_date
        );
        $this->db->insert('rmo_history',$add);
        $add= array(
          'rm_id' =>$rm_id,
          'outlet_id' =>$outlet_id,
          'used_stock'=>0,
          'total_stock'=>-1*$old_qty,
          'create_by'=>$this->session->userdata('user_id'),
          'create_date'=>date('Y-m-d H:i:s'),
          'i_date'  => $i_date
        );
        $this->db->insert('rmo_history',$add);
      }
      else if($type=='return_order')
      {
        $add= array(
          'rm_id' =>$rm_id,
          'outlet_id' =>$outlet_id,
          'used_stock'=>0,
          'total_stock'=>-1*$qty,
          'create_by'=>$this->session->userdata('user_id'),
          'create_date'=>date('Y-m-d H:i:s'),
          'i_date'  => $i_date
        );
        $this->db->insert('rmo_history',$add);
      }
      else if($type=='pos')
      {
        $add= array(
          'rm_id' =>$rm_id,
          'outlet_id' =>$outlet_id,
          'used_stock'=>$qty,
          'total_stock'=>0,
          'create_by'=>$this->session->userdata('user_id'),
          'create_date'=>date('Y-m-d H:i:s'),
          'i_date'  => $i_date
        );
        $this->db->insert('rmo_history',$add);
      }
    }
}
