<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Raw_supplier extends CI_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Raw_model');
        $this->load->model('Raw_supplier_model');
        $this->load->model('Outlet_model');
        $this->load->model('Supplier_model');
        $this->load->model('Constant_model');
        $this->load->model('Raw_material_outlet_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('pagination');

        $settingResult = $this->db->get_where('site_setting');
        $settingData = $settingResult->row();

        $setting_timezone = $settingData->timezone;

        date_default_timezone_set("$setting_timezone");
    }

    public function index()
    {
        $search=array();
        if(isset($_GET['supplier'])){
            $search['supplier_id']=$_GET['supplier'];
        }
        if(isset($_GET['outlet'])){
            $search['outlet_id']=$_GET['outlet'];
        }
        if(isset($_GET['raw'])){
            $search['raw_id']=$_GET['raw'];
        }
        if(isset($_GET['start_date']) && !empty($_GET['start_date'])){
            $search['startDate']=$_GET['start_date'];
            $search['startDate']= date("Y-m-d", strtotime($search['startDate']));
        }
        if(isset($_GET['end_date']) && !empty($_GET['end_date'])){
            $search['endDate']=$_GET['end_date'];
            $search['endDate']= date("Y-m-d", strtotime($search['endDate']));
        }
        $s= $this->lang_translator->get_translate();
        $data =$s;
        $paginationData = $this->Constant_model->getDataOneColumn('site_setting', 'id', '1');
        $pagination_limit = $paginationData[0]->pagination;
        $setting_dateformat = $paginationData[0]->datetime_format;

        if ($setting_dateformat == 'Y-m-d') {
            $dateformat = 'yyyy-mm-dd';
        }
        if ($setting_dateformat == 'Y.m.d') {
            $dateformat = 'yyyy.mm.dd';
        }
        if ($setting_dateformat == 'Y/m/d') {
            $dateformat = 'yyyy/mm/dd';
        }
        if ($setting_dateformat == 'm-d-Y') {
            $dateformat = 'mm-dd-yyyy';
        }
        if ($setting_dateformat == 'm.d.Y') {
            $dateformat = 'mm.dd.yyyy';
        }
        if ($setting_dateformat == 'm/d/Y') {
            $dateformat = 'mm/dd/yyyy';
        }
        if ($setting_dateformat == 'd-m-Y') {
            $dateformat = 'dd-mm-yyyy';
        }
        if ($setting_dateformat == 'd.m.Y') {
            $dateformat = 'dd.mm.yyyy';
        }
        if ($setting_dateformat == 'd/m/Y') {
            $dateformat = 'dd/mm/yyyy';
        }

        $data['dateformat'] = $dateformat;
        $data['display_dateformat'] = $setting_dateformat;
        $config['reuse_query_string'] = true;
          $config['base_url'] = base_url().'raw_supplier/';
          $config['total_rows'] = $this->Raw_supplier_model->count_selectAll($search);//total data
          $config['per_page'] = 20;
          $config['enable_query_strings'] = true;
          $config['first_link']       = 'First';
          $config['last_link']        = 'Last';
          $config['next_link']        = 'Next';
          $config['prev_link']        = 'Prev';
          $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
          $config['full_tag_close']   = '</ul></nav></div>';
          $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
          $config['num_tag_close']    = '</span></li>';
          $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
          $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
          $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
          $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['prev_tagl_close']  = '</span>Next</li>';
          $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
          $config['first_tagl_close'] = '</span></li>';
          $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['last_tagl_close']  = '</span></li>';
          $last = $this->uri->total_segments();
          $from = $this->uri->segment($last);
          $this->pagination->initialize($config);
          $data['raw_supplier_list']=  $this->Raw_supplier_model->selectAll($search,$config['per_page'],$from , true);
           $data['list_supplier'] = $this->Supplier_model->selectAll();
        //print_r($data['list_supplier']);exit;
          $data['list_outlet'] = $this->Outlet_model->selectAll();
          $data['list_raw'] = $this->Raw_model->selectAll();
          $this->load->view('view_raw_supplier_index', $data);
    }

    public function edit($raw_suplier_id=null)

    {
        $s= $this->lang_translator->get_translate();
        $data =$s;
        $data['raw_supplier_list'] = $this->Raw_supplier_model->getDetail(array('rmos_id'=>$raw_suplier_id));
         $data['list_supplier'] = $this->Supplier_model->selectAll();
        //print_r($data['list_supplier']);exit;
        $data['list_outlet'] = $this->Outlet_model->selectAll();
        $data['list_raw'] = $this->Raw_model->selectAll();
        $this->load->view('edit_raw_supplier', $data);
    }

    public function edit_submit($raw_supplier_id=null)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {
            $data_to_update=array(
                'outlet_id'       =>$this->input->post('outlet'),
                'supplier_id'       =>$this->input->post('supplier'),
                'qty'           =>$this->input->post('qty'),
                'update_by'     =>$this->session->userdata('user_id'),
                'update_date'   =>date('Y-m-d H:i:s'),
                'raw_id'        =>$this->input->post('raw'),
                'price_per_qty' => $this->input->post('price'),
                'trx_date'      => $this->input->post('trx_date')
            );
            $data_where=array(
                'rmos_id' =>$raw_supplier_id
            );
            //get current stock
            $curr_data= $this->db->select('*')->from('raw_m_outlet_supplier')->where('is_active',true)->where('rmos_id',$raw_supplier_id)->get()->row()->qty;
            $diff =  $data_to_update['qty']- $curr_data;
            $proc =  $this->db->set('total_stock','total_stock+'.$diff,FALSE)->where('outlet_id',$data_to_update['outlet_id'])->where('rm_id',$data_to_update['raw_id'])->update('raw_material_outlet');
            //insert into log
            $this->Raw_material_outlet_model->insertIntoRMOHistory($data_to_update['raw_id'], $data_to_update['outlet_id'] , 'update_from_supplier', $data_to_update['qty'],$curr_data);
            $q = $this->Raw_supplier_model->saveEdit($data_to_update, $data_where);
            if($q)
            {
                 $this->core_helper->setFlashAlert('Berhasil update bahan mentah' , 'SUCCESS' , true);
            }
         }
    }

    public function delete($raw__supplier_id)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {
             $data_to_update=array(
                'is_active' => false,
                'update_by' =>$this->session->userdata('user_id'),
                'update_date'=>date('Y-m-d H:i:s')
            );
            $data_where=array(
                'rmos_id' =>$raw__supplier_id
            );
            $q = $this->Raw_supplier_model->saveEdit($data_to_update, $data_where);
            if($q)
            {
                 $this->core_helper->setFlashAlert('Berhasil delete data' , 'SUCCESS' , true);
            }
         }
    }

    public function add()
    {
        $s= $this->lang_translator->get_translate();
        $data =$s;
        $data['list_supplier'] = $this->Supplier_model->selectAll();
        //print_r($data['list_supplier']);exit;
        $data['list_outlet'] = $this->Outlet_model->selectAll();
        $data['list_raw'] = $this->Raw_model->selectAll();
        $this->load->view('add_raw_supplier', $data);
    }

    public function add_submit()
    {

        if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {
            $data_to_add=array(
                'outlet_id'       =>$this->input->post('outlet'),
                'supplier_id'       =>$this->input->post('supplier'),
                'qty'           =>$this->input->post('qty'),
                'create_by'     =>$this->session->userdata('user_id'),
                'create_date'   =>date('Y-m-d H:i:s'),
                'is_active'     =>true,
                'raw_id'        =>$this->input->post('raw'),
                'price_per_qty' => $this->input->post('price'),
                'trx_date'      => $this->input->post('trx_date')
            );
            //check
            $c =  $this->db->select('*')->from('raw_m_outlet_supplier')->where('is_active',true)->where('outlet_id',$data_to_add['outlet_id'])->where('supplier_id',$data_to_add['supplier_id'])->where('raw_id',$data_to_add['raw_id'])->get()->result();
            //if(empty($c))
            //{
              //
              $check =  $this->db->select('*')->from('raw_material_outlet')->where('is_active',true)->where('rm_id',$data_to_add['raw_id'])->where('outlet_id',$data_to_add['outlet_id'])->get()->row();
              if(empty($check))
              {
                //add into this tabel
                $add_to_raw_outlet=array(
                  'rm_id' => $data_to_add['raw_id'],
                  'outlet_id'=> $data_to_add['outlet_id'],
                  'used_stock'  =>0,
                  'total_stock' => $data_to_add['qty'],
                  'is_active' =>true,
                  'create_date' => date('Y-m-d H:i:s'),
                  'create_by' => $this->session->userdata('user_id')
                );
                $this->Raw_material_outlet_model->add($add_to_raw_outlet);
                
              }
              else{
                //edit this tabel
                $data_to_update=array(
                  'total_stock' => $data_to_add['qty'],
                  'update_date' => date('Y-m-d H:i:s'),
                  'update_by' =>$this->session->userdata('user_id') 
                );
                $data_where=array(
                  'rm_id' => $data_to_add['raw_id'],
                  'outlet_id'=> $data_to_add['outlet_id']
                );
                $proc =  $this->db->set('total_stock','total_stock+'.$data_to_add['qty'],FALSE)->where('outlet_id',$data_to_add['outlet_id'])->where('rm_id',$data_to_add['raw_id'])->update('raw_material_outlet');
                //insert into log
                $this->Raw_material_outlet_model->insertIntoRMOHistory($data_to_add['raw_id'], $data_to_add['outlet_id'] , 'add_from_supplier', $data_to_add['qty']);
                //$this->Raw_material_outlet_model->saveEdit($data_to_update,$data_where);
              }
              $q = $this->Raw_supplier_model->add_data($data_to_add);
              if($q)
              {
                   $this->core_helper->setFlashAlert('Berhasil tambah bahan mentah' , 'SUCCESS' , true);
              }
            //}
            //else{
              //  $this->core_helper->setFlashAlert('Data sudah ada' , 'WARNING' , true);
            //}
            
         }
    }

    public function excel()
    {
       $paginationData = $this->Constant_model->getDataOneColumn('site_setting', 'id', '1');
        $pagination_limit = $paginationData[0]->pagination;
        $setting_dateformat = $paginationData[0]->datetime_format;
        $this->load->library('excel');
        require_once './application/third_party/PHPExcel.php';
        require_once './application/third_party/PHPExcel/IOFactory.php';
        $objPHPExcel = new PHPExcel();
        $default_border = array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000'),
        );
        $acc_default_border = array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => 'c7c7c7'),
        );
        $raw_style_header = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 10,
                'name' => 'Arial',
                'bold' => true,
            ),
        );
        $top_header_style = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 15,
                'name' => 'Arial',
                'bold' => true,
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
        $style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
                'bold' => true,
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
        );
        $account_value_style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
        );
        $text_align_style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
                'bold' => true,
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "Laporan bahan mentah");
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "Tanggal");
        $objPHPExcel->getActiveSheet()->setCellValue('B2', "Supplier");
        $objPHPExcel->getActiveSheet()->setCellValue('C2', "Outlet");
        $objPHPExcel->getActiveSheet()->setCellValue('D2', "Nama Bahan Mentah");
        $objPHPExcel->getActiveSheet()->setCellValue('E2', "Unit");
        $objPHPExcel->getActiveSheet()->setCellValue('F2', "Total barang");
        $objPHPExcel->getActiveSheet()->setCellValue('G2', "Harga satuan");
        $objPHPExcel->getActiveSheet()->setCellValue('H2', "Total harga");
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $jj = 3;
        $search=array();
        if(isset($_GET['supplier'])){
            $search['supplier_id']=$_GET['supplier'];
        }
        if(isset($_GET['outlet'])){
            $search['outlet_id']=$_GET['outlet'];
        }
        if(isset($_GET['raw'])){
            $search['raw_id']=$_GET['raw'];
        }
        if(isset($_GET['start_date']) && !empty($_GET['start_date'])){
            $search['startDate']=$_GET['start_date'];
            $search['startDate']= date("Y-m-d", strtotime($search['startDate']));
        }
        if(isset($_GET['end_date']) && !empty($_GET['end_date'])){
            $search['endDate']=$_GET['end_date'];
            $search['endDate']= date("Y-m-d", strtotime($search['endDate']));
        }
        $data= $this->Raw_supplier_model->selectAll($search);
        foreach($data as $d)
        {
            $trx_date       = $d->trx_date;
            $supplier_name  = $d->supplier_name;
            $outlet_name    = $d->outlet_name;
            $rm_name        = $d->rm_name;
            $rm_unit        = $d->rm_unit;
            $qty            = $d->qty;
            $price_per_qty  = $this->core_helper->convertToCurrency($d->price_per_qty);
            $total_price    = $this->core_helper->convertToCurrency($d->qty*$d->price_per_qty);
            $objPHPExcel->getActiveSheet()->setCellValue("A$jj", "$trx_date");
            $objPHPExcel->getActiveSheet()->setCellValue("B$jj", "$supplier_name");
            $objPHPExcel->getActiveSheet()->setCellValue("C$jj", "$outlet_name");
            $objPHPExcel->getActiveSheet()->setCellValue("D$jj", "$rm_name");
            $objPHPExcel->getActiveSheet()->setCellValue("E$jj", "$rm_unit");
            $objPHPExcel->getActiveSheet()->setCellValue("F$jj", "$qty");
            $objPHPExcel->getActiveSheet()->setCellValue("G$jj", "$price_per_qty");
            $objPHPExcel->getActiveSheet()->setCellValue("H$jj", "$total_price");
            $jj++;
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Raw_Material_Report.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

}
