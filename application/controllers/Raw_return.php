<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Raw_return extends CI_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Raw_model');
        $this->load->model('Outlet_model');
        $this->load->model('Raw_return_model');
        $this->load->model('Raw_outlet_model');
        $this->load->model('Raw_material_outlet_model');

        $this->load->model('Payment_method_model');
        $this->load->model('Constant_model');
        $this->load->model('Raw_return_model');
        $this->load->model('Supplier_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('pagination');

        $settingResult = $this->db->get_where('site_setting');
        $settingData = $settingResult->row();

        $setting_timezone = $settingData->timezone;

        date_default_timezone_set("$setting_timezone");
    }

    public function index()
    {
        $search=array();
        $end_date='';
        if (isset($_GET['trx_no'])) {
            $search['trx_no'] = $_GET['trx_no'];
        }
        if (isset($_GET['raw_id'])) {
            $search['rm_id'] = $_GET['raw_id'];
        }
        if (isset($_GET['payment_method'])) {
            $search['payment_method_id'] = $_GET['payment_method'];
        }
        if (isset($_GET['outlet_id'])) {
            $search['outlet_id'] = $_GET['outlet_id'];
        }
        if(isset($_GET['start_date']) && !empty($_GET['start_date'])){
            $search['start_date']=$_GET['start_date'];
            $search['start_date']= date("Y-m-d", strtotime($search['start_date']));
        }
        if(isset($_GET['end_date']) && !empty($_GET['end_date'])){
            $search['end_date']=$_GET['end_date'];
            $search['end_date']= date("Y-m-d", strtotime($search['end_date']));
        }
        $s= $this->lang_translator->get_translate();
        $data =$s;
         $paginationData = $this->Constant_model->getDataOneColumn('site_setting', 'id', '1');
        $pagination_limit = $paginationData[0]->pagination;
        $setting_dateformat = $paginationData[0]->datetime_format;

        if ($setting_dateformat == 'Y-m-d') {
            $dateformat = 'yyyy-mm-dd';
        }
        if ($setting_dateformat == 'Y.m.d') {
            $dateformat = 'yyyy.mm.dd';
        }
        if ($setting_dateformat == 'Y/m/d') {
            $dateformat = 'yyyy/mm/dd';
        }
        if ($setting_dateformat == 'm-d-Y') {
            $dateformat = 'mm-dd-yyyy';
        }
        if ($setting_dateformat == 'm.d.Y') {
            $dateformat = 'mm.dd.yyyy';
        }
        if ($setting_dateformat == 'm/d/Y') {
            $dateformat = 'mm/dd/yyyy';
        }
        if ($setting_dateformat == 'd-m-Y') {
            $dateformat = 'dd-mm-yyyy';
        }
        if ($setting_dateformat == 'd.m.Y') {
            $dateformat = 'dd.mm.yyyy';
        }
        if ($setting_dateformat == 'd/m/Y') {
            $dateformat = 'dd/mm/yyyy';
        }
         $data['dateformat'] = $dateformat;
        $data['display_dateformat'] = $setting_dateformat;
        $config['reuse_query_string'] = true;
          $config['base_url'] = base_url().'raw_return/';
          $config['total_rows'] = $this->Raw_return_model->count_selectAll($search);//total data
          $config['per_page'] = 20;
          $config['enable_query_strings'] = true;
          $config['first_link']       = 'First';
          $config['last_link']        = 'Last';
          $config['next_link']        = 'Next';
          $config['prev_link']        = 'Prev';
          $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
          $config['full_tag_close']   = '</ul></nav></div>';
          $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
          $config['num_tag_close']    = '</span></li>';
          $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
          $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
          $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
          $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['prev_tagl_close']  = '</span>Next</li>';
          $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
          $config['first_tagl_close'] = '</span></li>';
          $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['last_tagl_close']  = '</span></li>';
          $last = $this->uri->total_segments();
          $from = $this->uri->segment($last);
          $this->pagination->initialize($config);
          $data['raw_return_list']=  $this->Raw_return_model->selectAll($search,$config['per_page'],$from , true);
        //print_r($data['list_supplier']);exit;
          $data['list_outlet'] = $this->Outlet_model->selectAll();
          $data['list_raw'] = $this->Raw_model->selectAll();
          if($this->session->userdata('user_role')==3)
          {
            $data['outlet'] = $this->Outlet_model->selectAllForKasir();
          }
          else{
            $data['outlet'] = $this->Outlet_model->selectAll();
          }
        $data['payment_list'] = $this->Payment_method_model->selectAll();
          $this->load->view('view_raw_return', $data);
    }

    public function edit($raw_return_id=null)

    {
        $s= $this->lang_translator->get_translate();
        $data =$s;
        $this->load->view('edit_raw_return', $data);
    }

    public function add(){
        $s= $this->lang_translator->get_translate();
        $data =$s;
        $outlet_id=null;
        if(isset($_GET['outlet_id']))
        {
          $outlet_id= $_GET['outlet_id'];
        }
        if($this->session->userdata('user_role')==3)
          {
            $data['outlet'] = $this->Outlet_model->selectAllForKasir();
          }
          else{
            $data['outlet'] = $this->Outlet_model->selectAll();
          }
        $data['payment_list'] = $this->Payment_method_model->selectAll();
        $data['raw_outlet'] = $this->Raw_outlet_model->selectAll(array('outlet_id'=>$outlet_id));
        $data['supplier'] = $this->Supplier_model->selectAll();
        $this->load->view('add_raw_return', $data);
    }

    public function add_submit()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {

            $trxHeader=array(
              'trx_no'          => date('ymdhis').rand(0,10000),
              'total_amount'   => $this->input->post('qty') * $this->input->post('price'),
              'trx_date'   => $this->input->post('trx_date'),
              'outlet_id'   => $this->input->post('outlet_id'),
              'create_date' => date('Y-m-d H:i:s'),
              'create_by' => $this->session->userdata('user_id'),
              'payment_method_id' => $this->input->post('payment_method')
            );
            $trxDetail=array(
              'trx_no'  => $trxHeader['trx_no'],
              'rm_id'   => $this->input->post('raw_id'),
              'create_date' => date('Y-m-d H:i:s'),
              'create_by' => $this->session->userdata('user_id'),
              'qty' => $this->input->post('qty'),
              'price_per_qty' => $this->input->post('price'),
              'reason' => $this->input->post('reason'),
              'supplier_id' => $this->input->post('supplier')
            );
            $proc =  $this->db->set('total_stock','total_stock-'.$trxDetail['qty'],FALSE)->where('outlet_id',$trxHeader['outlet_id'])->where('rm_id',$trxDetail['rm_id'])->update('raw_material_outlet');
            $this->Raw_material_outlet_model->insertIntoRMOHistory($trxDetail['rm_id'], $trxHeader['outlet_id'] , 'return_order', $trxDetail['qty']);
            $this->Raw_return_model->saveTrx($trxHeader , $trxDetail);
            $this->core_helper->setFlashAlert('Berhasil tambah data return bahan mentah' , 'SUCCESS' , true);
            
         }
    }

    public function edit_submit($raw_outlet_id=null)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {
            $dataToSave=array(
              'rm_id'   => $this->input->post('raw'),
              'outlet_id'   => $this->input->post('outlet'),
              'used_stock'   => $this->input->post('used_stock'),
              'total_stock'   => $this->input->post('total_stock'),
              'is_active' => true,
              'update_date' => date('Y-m-d H:i:s'),
              'update_by' => $this->session->userdata('user_id')
            );
            $dataWhere=array(
              'rmo_id' => $raw_outlet_id
            );
            //check

         }
    }

     public function excel()
    {
       $paginationData = $this->Constant_model->getDataOneColumn('site_setting', 'id', '1');
        $pagination_limit = $paginationData[0]->pagination;
        $setting_dateformat = $paginationData[0]->datetime_format;
        $this->load->library('excel');
        require_once './application/third_party/PHPExcel.php';
        require_once './application/third_party/PHPExcel/IOFactory.php';
        $objPHPExcel = new PHPExcel();
        $default_border = array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000'),
        );
        $acc_default_border = array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => 'c7c7c7'),
        );
        $raw_style_header = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 10,
                'name' => 'Arial',
                'bold' => true,
            ),
        );
        $top_header_style = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 15,
                'name' => 'Arial',
                'bold' => true,
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
        $style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
                'bold' => true,
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
        );
        $account_value_style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
        );
        $text_align_style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
                'bold' => true,
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "Laporan Return Bahan Mentah");
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "Tanggal Return ");
        $objPHPExcel->getActiveSheet()->setCellValue('B2', "Kode Retur");
        $objPHPExcel->getActiveSheet()->setCellValue('C2', "Nama Outlet");
        $objPHPExcel->getActiveSheet()->setCellValue('D2', "Nama Supplier");
        $objPHPExcel->getActiveSheet()->setCellValue('E2', "Nama Bahan Mentah");
        $objPHPExcel->getActiveSheet()->setCellValue('F2', "Total dikembalikan");
        $objPHPExcel->getActiveSheet()->setCellValue('G2', "Harga satuan");
        $objPHPExcel->getActiveSheet()->setCellValue('H2', "Total harga");
        $objPHPExcel->getActiveSheet()->setCellValue('I2', "Alasan");
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $jj = 3;
        $total=0;
        $search=array();
        $end_date='';
        if (isset($_GET['trx_no'])) {
            $search['trx_no'] = $_GET['trx_no'];
        }
        if (isset($_GET['raw_id'])) {
            $search['rm_id'] = $_GET['raw_id'];
        }
        if (isset($_GET['payment_method'])) {
            $search['payment_method_id'] = $_GET['payment_method'];
        }
        if (isset($_GET['outlet_id'])) {
            $search['outlet_id'] = $_GET['outlet_id'];
        }
        if(isset($_GET['start_date']) && !empty($_GET['start_date'])){
            $search['start_date']=$_GET['start_date'];
            $search['start_date']= date("Y-m-d", strtotime($search['start_date']));
        }
        if(isset($_GET['end_date']) && !empty($_GET['end_date'])){
            $search['end_date']=$_GET['end_date'];
            $search['end_date']= date("Y-m-d", strtotime($search['end_date']));
        }
        $data= $this->Raw_return_model->selectAll($search);
        $total=0;
        foreach($data as $d)
        {
            $trx_date       = $d->trx_date;
            $trx_no  = $d->trx_no;
            $outlet_name    = $d->name;
            $supplier_name  = $d->supplier_name;
            $rm_name        = $d->rm_name;
            $rm_unit        = $d->rm_unit;
            $qty            = $d->qty;
            $price_per_qty  = $d->price_per_qty;
            $total_price    = $d->total_amount;
            $total = $total +$total_price;
            $price_per_qty  = $this->core_helper->convertToCurrency($d->price_per_qty);
            $total_price    = $this->core_helper->convertToCurrency($d->total_amount);
            $reason            = $d->reason;
            $objPHPExcel->getActiveSheet()->setCellValue("A$jj", "$trx_date");
            $objPHPExcel->getActiveSheet()->setCellValue("B$jj", "$trx_no");
            $objPHPExcel->getActiveSheet()->setCellValue("C$jj", "$outlet_name");
            $objPHPExcel->getActiveSheet()->setCellValue("D$jj", "$supplier_name");
            $objPHPExcel->getActiveSheet()->setCellValue("E$jj", "$rm_name");
            $objPHPExcel->getActiveSheet()->setCellValue("F$jj", "$qty"." $rm_unit");
            $objPHPExcel->getActiveSheet()->setCellValue("G$jj", "$price_per_qty");
            $objPHPExcel->getActiveSheet()->setCellValue("H$jj", "$total_price");
            $objPHPExcel->getActiveSheet()->setCellValue("I$jj", "$reason");
            
            $jj++;
        }
        $objPHPExcel->getActiveSheet()->setCellValue("G$jj", "Total pengembalian");
        $objPHPExcel->getActiveSheet()->setCellValue("H$jj", $this->core_helper->convertToCurrency($total));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$jj)->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$jj)->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('C'.$jj)->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('D'.$jj)->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('E'.$jj)->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('F'.$jj)->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('G'.$jj)->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('H'.$jj)->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('I'.$jj)->applyFromArray($style_header);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Material_Return_Report.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}
