<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Raw_outlet extends CI_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Raw_model');
        $this->load->model('Raw_supplier_model');
        $this->load->model('Raw_material_outlet_model');
        $this->load->model('Outlet_model');
        $this->load->model('Supplier_model');
        $this->load->model('Constant_model');
        $this->load->model('Raw_material_outlet_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('pagination');

        $settingResult = $this->db->get_where('site_setting');
        $settingData = $settingResult->row();

        $setting_timezone = $settingData->timezone;

        date_default_timezone_set("$setting_timezone");
    }

    public function index()
    {
        $search=array();
        if(isset($_GET['outlet'])){
            $search['outlet_id']=$_GET['outlet'];
        }
        $s= $this->lang_translator->get_translate();
        $data =$s;
        $config['reuse_query_string'] = true;
          $config['base_url'] = base_url().'raw_outlet/';
          $config['total_rows'] = $this->Raw_material_outlet_model->count_selectAll($search);//total data
          $config['per_page'] = 20;
          $config['enable_query_strings'] = true;
          $config['first_link']       = 'First';
          $config['last_link']        = 'Last';
          $config['next_link']        = 'Next';
          $config['prev_link']        = 'Prev';
          $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
          $config['full_tag_close']   = '</ul></nav></div>';
          $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
          $config['num_tag_close']    = '</span></li>';
          $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
          $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
          $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
          $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['prev_tagl_close']  = '</span>Next</li>';
          $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
          $config['first_tagl_close'] = '</span></li>';
          $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['last_tagl_close']  = '</span></li>';
          $last = $this->uri->total_segments();
          $from = $this->uri->segment($last);
          $this->pagination->initialize($config);
          $data['raw_outlet_list']=  $this->Raw_material_outlet_model->selectAll($search,$config['per_page'],$from , true);
        //print_r($data['list_supplier']);exit;
          if($this->session->userdata('user_role')==3)
          {
            $data['list_outlet'] = $this->Outlet_model->selectAllForKasir();
          }
          else{
            $data['list_outlet'] = $this->Outlet_model->selectAll();
          }
          
          $data['list_raw'] = $this->Raw_model->selectAll();
          $this->load->view('view_raw_outlet', $data);
    }

    public function edit($raw_suplier_id=null)

    {
        $s= $this->lang_translator->get_translate();
        $data =$s;
        $data['raw_supplier_list'] = $this->Raw_material_outlet_model->getDetail(array('rmos_id'=>$raw_suplier_id));
        $data['list_raw'] = $this->Raw_model->selectAll();
        $this->load->view('edit_raw_supplier', $data);
    }

    public function add_submit()
    {
        /*if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {
            $dataToSave=array(
              'rm_id'   => $this->input->post('raw'),
              'outlet_id'   => $this->input->post('outlet'),
              'used_stock'   => $this->input->post('used_stock'),
              'total_stock'   => $this->input->post('total_stock'),
              'is_active' => true,
              'create_date' => date('Y-m-d H:i:s'),
              'create_by' => $this->session->userdata('user_id')
            );
            //check
            $check =  $this->db->select('*')->from('raw_material_outlet')->where('is_active',true)->where('rm_id',$dataToSave['rm_id'])->where('outlet_id',$dataToSave['outlet_id'])->get()->row();
              if(empty($check))
              {
                 $this->Raw_material_outlet_model->add($dataToSave);
                $this->core_helper->setFlashAlert('Berhasil menambah data' , 'SUCCESS' , true);
              }else{
                 $this->core_helper->setFlashAlert('Data sudah ada' , 'WARNING' , true);
              }
            
         }*/
    }

    public function edit_submit($raw_outlet_id=null)
    {
        /*if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {
            $dataToSave=array(
              'rm_id'   => $this->input->post('raw'),
              'outlet_id'   => $this->input->post('outlet'),
              'used_stock'   => $this->input->post('used_stock'),
              'total_stock'   => $this->input->post('total_stock'),
              'is_active' => true,
              'update_date' => date('Y-m-d H:i:s'),
              'update_by' => $this->session->userdata('user_id')
            );
            $dataWhere=array(
              'rmo_id' => $raw_outlet_id
            );
            //check
            $check =  $this->db->select('*')->from('raw_material_outlet')->where('is_active',true)->where('rm_id',$dataToSave['rm_id'])->where('outlet_id',$dataToSave['outlet_id'])->where('rmo_id<>',$raw_outlet_id)->get()->row();
            if(empty($check))
            {
              //update
              $this->Raw_material_outlet_model->saveEdit($dataToSave,$dataWhere);
               $this->core_helper->setFlashAlert('Berhasil menyimpan data' , 'SUCCESS' , true);
            }
            else{
              $this->core_helper->setFlashAlert('Data sudah ada' , 'WARNING' , true);
            }
         }*/
    }

    public function delete_submit($raw_outlet_id=null)
    {
        /*if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {
            $data_to_update=array(
                'is_active' => false,
                'update_by' =>$this->session->userdata('user_id'),
                'update_date'=>date('Y-m-d H:i:s')
            );
            $data_where=array(
                'rmo_id' =>$raw_outlet_id
            );
            $q = $this->Raw_material_outlet_model->saveEdit($data_to_update, $data_where);
            if($q)
            {
                 $this->core_helper->setFlashAlert('Berhasil hapus data' , 'SUCCESS' , true);
            }
         }*/
    }

    public function search()
    {
      $search=array();
        if(isset($_GET['outlet_id'])){
            $search['outlet_id']=$_GET['outlet_id'];
        }
        if(isset($_GET['start_date'])){
            $search['start_date']=$_GET['start_date'];
        }
        if(isset($_GET['outlet_id'])){
            $search['outlet_id']=$_GET['outlet_id'];
        }
        if(isset($_GET['end_date'])){
            $search['end_date']=$_GET['end_date'];
        }
        $s= $this->lang_translator->get_translate();
        $data =$s;
          $data['raw_outlet_list']=  $this->Raw_material_outlet_model->selectAllHistory($search);
        //print_r($data['list_supplier']);exit;
          if($this->session->userdata('user_role')==3)
          {
            $data['list_outlet'] = $this->Outlet_model->selectAllForKasir();
          }
          else{
            $data['list_outlet'] = $this->Outlet_model->selectAll();
          }
          $this->load->view('raw_outlet_history', $data);
    }

    public function history_excel()
    {
      $paginationData = $this->Constant_model->getDataOneColumn('site_setting', 'id', '1');
        $pagination_limit = $paginationData[0]->pagination;
        $setting_dateformat = $paginationData[0]->datetime_format;
        $this->load->library('excel');
        require_once './application/third_party/PHPExcel.php';
        require_once './application/third_party/PHPExcel/IOFactory.php';
        $objPHPExcel = new PHPExcel();
        $default_border = array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000'),
        );
        $acc_default_border = array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => 'c7c7c7'),
        );
        $raw_style_header = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 10,
                'name' => 'Arial',
                'bold' => true,
            ),
        );
        $top_header_style = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 15,
                'name' => 'Arial',
                'bold' => true,
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
        $style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
                'bold' => true,
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
        );
        $account_value_style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
        );
        $text_align_style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
                'bold' => true,
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "Laporan stock bahan mentah per outlet");
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "Tanggal");
        $objPHPExcel->getActiveSheet()->setCellValue('B2', "Nama Outlet");
        $objPHPExcel->getActiveSheet()->setCellValue('C2', "Nama Bahan Mentah");
        $objPHPExcel->getActiveSheet()->setCellValue('D2', "Total stock");
        $objPHPExcel->getActiveSheet()->setCellValue('E2', "Total terpakai");
        $objPHPExcel->getActiveSheet()->setCellValue('F2', "Sisa");
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $jj = 3;
        $search=array();
        if(isset($_GET['start_date'])){
            $search['start_date']=$_GET['start_date'];
        }
        if(isset($_GET['outlet_id'])){
            $search['outlet_id']=$_GET['outlet_id'];
        }
        if(isset($_GET['end_date'])){
            $search['end_date']=$_GET['end_date'];
        }
        /*if(isset($_GET['start_date']) && !empty($_GET['start_date'])){
            $search['startDate']=$_GET['start_date'];
            $search['startDate']= date("Y-m-d", strtotime($search['startDate']));
        }
        if(isset($_GET['end_date']) && !empty($_GET['end_date'])){
            $search['endDate']=$_GET['end_date'];
            $search['endDate']= date("Y-m-d", strtotime($search['endDate']));
        }*/
        $data= $this->Raw_material_outlet_model->selectAllHistory($search);
        foreach($data as $d)
        {
            $date       = $d->i_date;
            $outlet_name    = $d->outlet_name;
            $rm_name        = $d->rm_name;
            $rm_unit        = $d->rm_unit;
            $total_stock            = $d->total_stock;
            $used_stock            = $d->used_stock;
           
            $objPHPExcel->getActiveSheet()->setCellValue("A$jj", "$date");
            $objPHPExcel->getActiveSheet()->setCellValue("B$jj", "$outlet_name");
            $objPHPExcel->getActiveSheet()->setCellValue("C$jj", "$rm_name");
            $objPHPExcel->getActiveSheet()->setCellValue("D$jj", "$total_stock"." ".$rm_unit);
            $objPHPExcel->getActiveSheet()->setCellValue("E$jj", "$used_stock"." ".$rm_unit);
            $objPHPExcel->getActiveSheet()->setCellValue("F$jj", "($total_stock-$used_stock)"." ".$rm_unit);
            $jj++;
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Raw_Material_Report.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }


}
