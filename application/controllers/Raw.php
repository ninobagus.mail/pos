<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Raw extends CI_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Raw_model');
        $this->load->model('Constant_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('pagination');

        $settingResult = $this->db->get_where('site_setting');
        $settingData = $settingResult->row();

        $setting_timezone = $settingData->timezone;

        date_default_timezone_set("$setting_timezone");
    }

    public function index()
    {
        $search=array();
        if(isset($_GET['search_name'])){
            $search['rm_name']=$_GET['search_name'];
        }
        if(isset($_GET['search_code'])){
            $search['rm_code']=$_GET['search_code'];
        }
        $s= $this->lang_translator->get_translate();
        $data =$s;
        $config['reuse_query_string'] = true;
          $config['base_url'] = base_url().'raw/';
          $config['total_rows'] = $this->Raw_model->count_selectAll($search);//total data
          $config['per_page'] = 20;
          $config['enable_query_strings'] = true;
          $config['first_link']       = 'First';
          $config['last_link']        = 'Last';
          $config['next_link']        = 'Next';
          $config['prev_link']        = 'Prev';
          $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
          $config['full_tag_close']   = '</ul></nav></div>';
          $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
          $config['num_tag_close']    = '</span></li>';
          $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
          $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
          $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
          $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['prev_tagl_close']  = '</span>Next</li>';
          $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
          $config['first_tagl_close'] = '</span></li>';
          $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
          $config['last_tagl_close']  = '</span></li>';
          $last = $this->uri->total_segments();
          $from = $this->uri->segment($last);
          $this->pagination->initialize($config);
          $data['raw_list']=  $this->Raw_model->selectAll($search,$config['per_page'],$from , true);
          $this->load->view('view_raw_index', $data);
    }

    public function edit($raw_id=null)

    {
        $s= $this->lang_translator->get_translate();
        $data =$s;
        $data['raw_list'] = $this->Raw_model->getDetail(array('rm_id'=>$raw_id));
        $this->load->view('edit_raw_index', $data);
    }

    public function edit_submit($raw_id=null)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {
            $data_to_update=array(
                'rm_name'   =>$this->input->post('rm_name'),
                'rm_code'   =>$this->input->post('rm_code'),
                'rm_unit'   =>$this->input->post('rm_unit'),
                'update_by' =>$this->session->userdata('user_id'),
                'update_date'=>date('Y-m-d H:i:s')
            );
            $data_where=array(
                'rm_id' =>$raw_id
            );
            $q = $this->Raw_model->saveEdit($data_to_update, $data_where);
            if($q)
            {
                 $this->core_helper->setFlashAlert('Berhasil update bahan mentah' , 'SUCCESS' , true);
            }
         }
    }

    public function delete($raw_id)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {
             $data_to_update=array(
                'is_active' => false,
                'update_by' =>$this->session->userdata('user_id'),
                'update_date'=>date('Y-m-d H:i:s')
            );
            $data_where=array(
                'rm_id' =>$raw_id
            );
            $q = $this->Raw_model->saveEdit($data_to_update, $data_where);
            if($q)
            {
                 $this->core_helper->setFlashAlert('Berhasil delete bahan mentah' , 'SUCCESS' , true);
            }
         }
    }

    public function raw_add()
    {
        $s= $this->lang_translator->get_translate();
        $data =$s;
        $this->load->view('add_raw', $data);
    }

    public function add_submit()
    {

        if ($_SERVER['REQUEST_METHOD'] === 'POST')
         {
            $data_to_add=array(
                'rm_name'       =>$this->input->post('rm_name'),
                'rm_code'       =>$this->input->post('rm_code'),
                'rm_unit'       =>$this->input->post('rm_unit'),
                'create_by'     =>$this->session->userdata('user_id'),
                'create_date'   =>date('Y-m-d H:i:s'),
                'is_active'    =>true
            );

            $q = $this->Raw_model->add_data($data_to_add);
            if($q)
            {
                 $this->core_helper->setFlashAlert('Berhasil tambah bahan mentah' , 'SUCCESS' , true);
            }
         }
    }

}
