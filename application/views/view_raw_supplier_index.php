<?php
    require_once 'includes/header.php';
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script>
	$( function() {
		$( "#startDate" ).datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});
		
		$("#endDate").datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});
	} );
</script>

<!-- Add jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo "raw material supplier - view" ?></h1>
		</div>
	</div><!--/.row-->
	<?php
    $supplier = '';
    $outlet='';
    $raw = '';
    
    $start_date='';
    $end_date='';
    $get='?';
    if (isset($_GET['supplier'])) {
        $supplier = $_GET['supplier'];
        $get=$get.'supplier='.$supplier.'&';
    }
    if (isset($_GET['outlet'])) {
        $outlet = $_GET['outlet'];
        $get=$get.'outlet='.$outlet.'&';
    }
    if (isset($_GET['raw'])) {
        $raw = $_GET['raw'];
        $get=$get.'raw='.$raw.'&';
    }
    if (isset($_GET['outlet_id'])) {
        $outlet_id = $_GET['outlet_id'];
        $get=$get.'outlet_id='.$outlet_id.'&';
    }
    if (isset($_GET['start_date'])) {
        $start_date = $_GET['start_date'];
        $get=$get.'start_date='.$start_date.'&';
    }
    if (isset($_GET['end_date'])) {
        $end_date = $_GET['end_date'];
        $get=$get.'end_date='.$end_date.'&';
    }
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="error-message">
	                 <?php $this->core_helper->showFlashAlert(); ?>                    
	                 <?php echo validation_errors(); ?>
	               </div>
					<form action="<?=base_url()?>raw_supplier" method="get">
						<div class="row" style="margin-top: 10px;">
							<div class="col-md-4">
								<div class="form-group">
									<p>Pilih supplier</p>
									<select class="form-control" name="supplier" >
										<option value="">--PILIH SEMUA--</option>
										<?php foreach($list_supplier as $s){ ?>
											
											<option <?php if($s->id==$supplier){echo "selected";} ?> value="<?php echo $s->id ?>"><?php echo $s->name; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<p>Pilih Outlet</p>
									<select class="form-control" name="outlet" >
										<option value="">--PILIH SEMUA--</option>
										<?php foreach($list_outlet as $s){ ?>
											<option <?php if($s->id==$outlet){echo "selected";} ?> value="<?php echo $s->id ?>"><?php echo $s->name. "( ".$s->address." )"; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<p>Pilih Material</p>
									<select class="form-control" name="raw" >
										<option value="">--PILIH SEMUA--</option>
										<?php foreach($list_raw as $s){ ?>
											<option <?php if($s->rm_id==$raw){echo "selected";} ?> value="<?php echo $s->rm_id ?>"><?php echo $s->rm_name; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?php echo "Dari tanggal" ?></label>
									<input type="text" value="<?php echo $start_date ?>" name="start_date" class="form-control" id="startDate" style="height: 35px" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label><?php echo "Sampai tanggal" ?></label>
									<input type="text" value="<?php echo $end_date ?>"  name="end_date" class="form-control" id="endDate" style="height: 35px" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<div style="height: 25px"></div>
									<button class="btn btn-info" type="submit">Search</button>&nbsp;
									<a href="<?php echo base_url('raw_supplier/excel'.$get) ?>" class="btn btn-success">Export excel</a>
								</div>
							</div>

							
						</div>
					</form>
					
					<div class="row" style="margin-top: 0px;">
						<div class="col-md-12">
							
						<div class="table-responsive">
							<table class="table">
							    <thead>
							    	<tr>
							    		<th ><?php echo 'Tanggal'; ?></th>
								    	<th ><?php echo 'Supplier'; ?></th>
								    	<th ><?php echo 'Outlet'; ?></th>
                                        <th ><?php echo 'Nama Bahan Mentah'; ?></th>
									    <th ><?php echo 'Unit'; ?></th>
									    <th ><?php echo 'Total barang'; ?></th>
									    <th ><?php echo 'Harga satuan'; ?></th>
									     <th ><?php echo 'Total Harga'; ?></th>
									     <th>Action</th>
									</tr>
							    </thead>
								<tbody>
									<?php foreach($raw_supplier_list as $s){ ?>
										<tr>
											<td><?php echo $s->trx_date; ?></td>
											<td><?php echo $s->supplier_name; ?></td>
											<td><?php echo $s->outlet_name; ?></td>
											<td><?php echo $s->rm_name; ?></td>
											<td><?php echo $s->rm_unit; ?></td>
											<td><?php echo $s->qty; ?></td>
											<td><?php echo $this->core_helper->convertToCurrency($s->price_per_qty); ?></td>
											<td><?php echo $this->core_helper->convertToCurrency($s->qty * $s->price_per_qty); ?></td>
											<td>
												<div class="row">
                                                <div class="col-sm-6">
                                                    <a href="<?php echo base_url('raw_supplier/edit/'.$s->rmos_id) ?>" class="btn btn-info">Edit</a>
                                                </div>
                                                <div class="col-sm-6">
                                                    <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-edit-raw-supplier-<?php echo $s->rmos_id ?>">Hapus</button>
                                                    <!-- Modal -->
                                                    <div id="modal-edit-raw-supplier-<?php echo $s->rmos_id ?>" class="modal fade" role="dialog">
                                                      <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Hapus data ?</h4>
                                                          </div>
                                                          <div class="modal-body">
                                                            <form class="" method="POST" action="<?php echo base_url('raw_supplier/delete/'.$s->rmos_id) ?>">
                                                                <div>
                                                                    <p>Anda yakin ingin menghapus data ini ? </p>
                                                                </div>
                                                                
                                                                <div>
                                                                    <button class="btn btn-warning" type="submit">Hapus</button>
                                                                    <button class="btn btn-deafult" data-dismiss="modal" type="submit">Batalkan</button>
                                                                </div>
                                                            </form>
                                                          </div>
                                                        </div>

                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
											</td>
										</tr>
									<?php } ?>
                                    
								</tbody>
							</table>
                            <?php echo $this->pagination->create_links(); ?>
						</div>
							
						</div>
					</div>
					
				</div><!-- Panel Body // END -->
			</div><!-- Panel Default // END -->
		</div><!-- Col md 12 // END -->
	</div><!-- Row // END -->
	
	<br /><br /><br />
	
</div><!-- Right Colmn // END -->
	
	
	
<?php
    require_once 'includes/footer.php';
?>