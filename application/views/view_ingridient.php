<?php
    require_once 'includes/header.php';
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script>
	$( function() {
		$( "#startDate" ).datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});
		
		$("#endDate").datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});
	} );
</script>

<!-- Add jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo "Ingridient" ?></h1>
		</div>
	</div><!--/.row-->

<?php
$rm_name='';
$get='';
if (isset($_GET['rm_name'])) {
        $rm_name = $_GET['rm_name'];
        $get=$get.'rm_name='.$rm_name.'&';
    }
?>
	
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="error-message">
	                 <?php $this->core_helper->showFlashAlert(); ?>                    
	                 <?php echo validation_errors(); ?>
	               </div>
					<?php if(!empty($product)){ ?>
						<h4><b>Detail produk</b></h4>
						<div>
							<p>Nama Produk : <?php echo $product->name; ?></p>
						</div>
						<div>
							<p>Kode Produk : <?php echo $product->code; ?></p>
						</div>
						<div>
							<p>Harga Produk / purchase price : <?php echo $this->core_helper->convertToCurrency($product->purchase_price) ; ?></p>
						</div>
						<div>
							<p>Harga Toko / retail price : <?php echo $this->core_helper->convertToCurrency($product->retail_price) ; ?></p>
						</div>
						<br>
						<?php if($this->session->userdata('user_role')==1){ ?>
						<div class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tambah ingridient</div>

						<!-- Modal -->
						<div id="myModal" class="modal fade" role="dialog">
						  <div class="modal-dialog">

						    <!-- Modal content-->
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						        <h4 class="modal-title">Tambah ingridient</h4>
						      </div>
						      <div class="modal-body">
						        <form action="<?php echo base_url('products/add_ingridient/'.$product_id) ?>" method="POST">
						        	<div class="form-group">
						        		<p>Pilih bahan mentah</p>
						        		<select name="rm_id" class="form-control" required="">
						        			<?php foreach($raw_list as $s){ ?>
						        				<option value="<?php echo $s->rm_id ?>"><?php echo $s->rm_name; ?></option>
						        			<?php } ?>
						        		</select>
						        	</div>
						        	<div class="form-group">
						        		<p>Jumlah digunakan per product ( Qty )</p>
						        		<input type="number" name="qty" required="" class="form-control">
						        	</div>
						        	<div class="form-group">
						        		<button type="submit" class="btn btn-primary">Simpan</button>
						        	</div>
						        </form>
						      </div>
						    </div>

						  </div>
						</div>
						<?php } ?>
						<br><br>
						
						<div class="row" style="margin-top: 0px;">
							<div class="col-md-12">
								
							<div class="table-responsive">
								<table class="table">
								    <thead>
								    	<tr>
								    		<th width="16%"><?php echo 'Nama Ingridient'; ?></th>
								    		<th width="16%"><?php echo 'Jumlah (Qty) per sajian'; ?></th>
								    		<th width="16%"><?php echo 'Satuan'; ?></th>
										    <th width="16%"><?php echo 'Action'; ?></th>
										</tr>
								    </thead>
									<tbody>
										<?php foreach($ingridient as $s){ ?>
											<tr>
												<td><?php echo $s->rm_name; ?></td>
												<td><?php echo $s->qty; ?></td>
												<td><?php echo $s->rm_unit; ?></td>
												<td>
													<div class="row">
														<div class="col-sm-6">
															<div class="btn btn-primary" data-toggle="modal" data-target="#modal-edit-ingridient<?php echo $s->rmp_id ?>">Edit ingridient</div>
															<!-- Modal -->
															<div id="modal-edit-ingridient<?php echo $s->rmp_id ?>" class="modal fade" role="dialog">
															  <div class="modal-dialog">

															    <!-- Modal content-->
															    <div class="modal-content">
															      <div class="modal-header">
															        <button type="button" class="close" data-dismiss="modal">&times;</button>
															        <h4 class="modal-title">Edit ingridient</h4>
															      </div>
															      <div class="modal-body">
															        <form action="<?php echo base_url('products/edit_ingridient_submit/'.$s->rmp_id) ?>" method="POST">
															        	<input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
															        	<div class="form-group">
															        		<p>Pilih bahan mentah</p>
															        		<select name="rm_id" class="form-control" required="">
															        			<?php foreach($raw_list as $r){ ?>
															        				<option <?php if($r->rm_id==$s->rm_id){echo "selected";} ?> value="<?php echo $r->rm_id ?>"><?php echo $r->rm_name; ?></option>
															        			<?php } ?>
															        		</select>
															        	</div>
															        	<div class="form-group">
															        		<p>Jumlah digunakan per product ( Qty )</p>
															        		<input type="number" name="qty" required="" class="form-control" value="<?php  echo $s->qty; ?>">
															        	</div>
															        	<div class="form-group">
															        		<button type="submit" class="btn btn-primary">Simpan perubahan</button>
															        	</div>
															        </form>
															      </div>
															    </div>

															  </div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="btn btn-danger" data-toggle="modal" data-target="#modal-del-ingridient<?php echo $s->rmp_id ?>">Hapus ingridient</div>
															<!-- Modal -->
															<div id="modal-del-ingridient<?php echo $s->rmp_id ?>" class="modal fade" role="dialog">
															  <div class="modal-dialog">

															    <!-- Modal content-->
															    <div class="modal-content">
															      <div class="modal-header">
															        <button type="button" class="close" data-dismiss="modal">&times;</button>
															        <h4 class="modal-title">Hapus ingridient</h4>
															      </div>
															      <div class="modal-body">
															        <form class="" method="POST" action="<?php echo base_url('products/delete_ingridient_submit/'.$s->rmp_id) ?>">
										                                <div>
			                                                                    <p>Anda yakin ingin menghapus data <b><?php echo $s->rm_name; ?></b> pada produk ini ? </p>
			                                                                </div>
			                                                                
			                                                                <div>
			                                                                    <button class="btn btn-warning" type="submit">Hapus</button>
			                                                                    <button class="btn btn-deafult" data-dismiss="modal" type="submit">Batalkan</button>
			                                                                </div>
		                                                            </form>
															      </div>
															    </div>

															  </div>
														</div>
													</div>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
	                            <?php echo $this->pagination->create_links(); ?>
							</div>
								
							</div>
						</div>

					<?php } ?>
					
				</div><!-- Panel Body // END -->
			</div><!-- Panel Default // END -->
		</div><!-- Col md 12 // END -->
	</div><!-- Row // END -->
	
	<br /><br /><br />
	
</div><!-- Right Colmn // END -->
	
	
	
<?php
    require_once 'includes/footer.php';
?>