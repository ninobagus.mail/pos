<?php
    require_once 'includes/header.php';
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script>
	$( function() {
		$( "#startDate" ).datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});
		
		$("#endDate").datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});
	} );
</script>

<!-- Add jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo "raw material - outlet" ?></h1>
		</div>
	</div><!--/.row-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="error-message">
	                 <?php $this->core_helper->showFlashAlert(); ?>                    
	                 <?php echo validation_errors(); ?>
	               </div>
	                <?php if($this->session->userdata('user_role')==1 && false){ ?>
						<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modaladd">Tambah Data</button>
	                                                    <!-- Modal -->
	                    <div id="modaladd" class="modal fade" role="dialog">
	                      <div class="modal-dialog">

	                        <!-- Modal content-->
	                        <div class="modal-content">
	                          <div class="modal-header">
	                            <button type="button" class="close" data-dismiss="modal">&times;</button>
	                            <h4 class="modal-title">Tambah data </b></h4>
	                          </div>
	                          <div class="modal-body">
	                            <form class="" method="POST" action="<?php echo base_url('raw_outlet/add_submit/') ?>">
	                                <div class="form-group">
	                                	<p>Pilih Material / Bahan mentah</p>
	                                	<select class="form-control" name="raw" required="">
	                                		<?php foreach($list_raw as $s){ ?>
	                                			<option value="<?php echo $s->rm_id ?>"><?php echo $s->rm_name; ?></option>
	                                		<?php } ?>
	                                	</select>
	                                </div>
	                                <div class="form-group">
	                                	<p>Pilih Outlet</p>
	                                	<select class="form-control" name="outlet" required="">
	                                		<?php foreach($list_outlet as $s){ ?>
	                                			<option value="<?php echo $s->id ?>"><?php echo $s->name; ?></option>
	                                		<?php } ?>
	                                	</select>
	                                </div>
	                                <div class="form-group">
	                                	<p>Stok Tersedia</p>
	                                	<input type="text" name="total_stock" required="" class="form-control">
	                                </div>
	                                <div class="form-group">
	                                	<p>Telah digunakan</p>
	                                	<input type="text" name="used_stock" required="" class="form-control">
	                                </div>
	                                <div class="form-group">
	                                	<button type="submit" class="btn btn-primary">Simpan</button>
	                                </div>
	                            </form>
	                          </div>
	                        </div>

	                      </div>
	                    </div>
	                <?php } ?>
					<form action="<?=base_url()?>raw_outlet" method="get">
						<?php $outlet=''; ?>
						<?php if(isset($_GET['outlet'])){$outlet=$_GET['outlet'];} ?>
						<div class="row" style="margin-top: 10px;">
							<div class="col-md-3">
								<div class="form-group">
									<label><?php echo 'Pilih outlet'; ?></label>
									<select name="outlet" class="form-control">
										<?php foreach($list_outlet as $s){ ?>
											<option <?php if($s->id==$outlet){echo "selected";} ?> value=<?php echo $s->id ?>> <?php echo $s->name; ?> </option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>&nbsp;</label><br />
									<input type="hidden" name="report" value="1" />
									<button class="btn btn-primary" style="width: 100%; height: 35px;">&nbsp;&nbsp;<?php echo "Search"; ?>&nbsp;&nbsp;</button>
								</div>
							</div>
						</div>
					</form>
					
					<div class="row" style="margin-top: 0px;">
						<div class="col-md-12">
							
						<div class="table-responsive">
							<?php if(isset($_GET['outlet']) && !empty($_GET['outlet'])){ ?>
								<a href="<?php echo base_url('raw_outlet/search?outlet_id='.$_GET['outlet']) ?>">Ke menu laporan bahan mentah per tanggal</a>
								<br><br>
								<table class="table">
								    <thead>
								    	<tr>
									    	<th width="16%"><?php echo 'Raw name'; ?></th>
									    	<th width="16%"><?php echo 'Total Supply'; ?></th>
	                                        <th width="16%"><?php echo 'Terpakai'; ?></th>
	                                        <th width="16%"><?php echo 'Sisa Stok'; ?></th>
										</tr>
								    </thead>
									<tbody>
										<?php foreach($raw_outlet_list as $s){ ?>
											<tr>
												<td><?php echo $s->rm_name; ?></td>
												<td><?php echo $s->total_stock." ".$s->rm_unit; ?></td>
												<td><?php echo $s->used_stock." ".$s->rm_unit; ?></td>
												<td><?php echo ($s->total_stock-$s->used_stock)." ".$s->rm_unit; ?></td>
												<?php if(false){ ?>
													<td>
														<div class="row">
															<div class="col-sm-6">
																<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-edit-raw-<?php echo $s->rmo_id ?>">Edit</button>
			                                                    <!-- Modal -->
			                                                    <div id="modal-edit-raw-<?php echo $s->rmo_id ?>" class="modal fade" role="dialog">
			                                                      <div class="modal-dialog">

			                                                        <!-- Modal content-->
			                                                        <div class="modal-content">
			                                                          <div class="modal-header">
			                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
			                                                            <h4 class="modal-title">Edit data </b></h4>
			                                                          </div>
			                                                          <div class="modal-body">
			                                                            <form class="" method="POST" action="<?php echo base_url('raw_outlet/edit_submit/'.$s->rmo_id) ?>">
											                                <div class="form-group">
											                                	<p>Pilih Material / Bahan mentah</p>
											                                	<select class="form-control" name="raw" required="">
											                                		<?php foreach($list_raw as $r){ ?>
											                                			<option <?php if($r->rm_id==$s->rm_id){echo "selected";} ?> value="<?php echo $r->rm_id ?>"><?php echo $r->rm_name; ?></option>
											                                		<?php } ?>
											                                	</select>
											                                </div>
											                                <div class="form-group">
											                                	<p>Pilih Outlet</p>
											                                	<select class="form-control" name="outlet" required="">
											                                		<?php foreach($list_outlet as $r){ ?>
											                                			<option <?php if($r->id==$s->outlet_id){echo "selected";} ?>  value="<?php echo $r->id ?>"><?php echo $r->name; ?></option>
											                                		<?php } ?>
											                                	</select>
											                                </div>
											                                <div class="form-group">
											                                	<p>Stok Tersedia</p>
											                                	<input type="text" name="total_stock" required="" class="form-control" value="<?php echo $s->total_stock ?>">
											                                </div>
											                                <div class="form-group">
											                                	<p>Telah digunakan</p>
											                                	<input type="text" name="used_stock" required="" class="form-control"  value="<?php echo $s->used_stock ?>">
											                                </div>
											                                <div class="form-group">
											                                	<button type="submit" class="btn btn-primary">Simpan</button>
											                                </div>
			                                                            </form>
			                                                          </div>
			                                                        </div>

			                                                      </div>
			                                                    </div>
															</div>
															<div class="col-sm-6">
																<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-del-raw-<?php echo $s->rmo_id ?>">Hapus</button>
			                                                    <!-- Modal -->
			                                                    <div id="modal-del-raw-<?php echo $s->rmo_id ?>" class="modal fade" role="dialog">
			                                                      <div class="modal-dialog">

			                                                        <!-- Modal content-->
			                                                        <div class="modal-content">
			                                                          <div class="modal-header">
			                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
			                                                            <h4 class="modal-title">Hapus data </b></h4>
			                                                          </div>
			                                                          <div class="modal-body">
			                                                            <form class="" method="POST" action="<?php echo base_url('raw_outlet/delete_submit/'.$s->rmo_id) ?>">
											                                <div>
				                                                                    <p>Anda yakin ingin menghapus data <b><?php echo $s->rm_name; ?></b> Pada Outlet ini  ? </p>
				                                                                </div>
				                                                                
				                                                                <div>
				                                                                    <button class="btn btn-warning" type="submit">Hapus</button>
				                                                                    <button class="btn btn-deafult" data-dismiss="modal" type="submit">Batalkan</button>
				                                                                </div>
			                                                            </form>
			                                                          </div>
			                                                        </div>

			                                                      </div>
			                                                    </div>
															</div>
														</div>
														
													</td>
												<?php } ?>
											</tr>
	                                    <?php } ?>
									</tbody>
								</table>
	                            <?php echo $this->pagination->create_links(); ?>
	                        <?php }else{ ?>
	                        	<div class="alert alert-info">Pilih outlet terlebih dahulu</div>
	                        <?php } ?>
						</div>
							
						</div>
					</div>
					
				</div><!-- Panel Body // END -->
			</div><!-- Panel Default // END -->
		</div><!-- Col md 12 // END -->
	</div><!-- Row // END -->
	
	<br /><br /><br />
	
</div><!-- Right Colmn // END -->
	
	
	
<?php
    require_once 'includes/footer.php';
?>