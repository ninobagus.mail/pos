<?php
    require_once 'includes/header.php';
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script>
	$( function() {
		$( "#startDate" ).datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});
		
		$("#endDate").datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});
	} );
</script>

<!-- Add jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo "raw material - outlet" ?></h1>
		</div>
	</div><!--/.row-->
	<div class="row">
		<?php 
		$get='?';
		$outlet_id='';
	    $raw = '';
	    
	    $start_date='';
	    $end_date='';
	    if (isset($_GET['start_date'])) {
	        $start_date = $_GET['start_date'];
	        $get=$get.'start_date='.$start_date.'&';
	    }
	    if (isset($_GET['outlet_id'])) {
	        $outlet = $_GET['outlet_id'];
	        $get=$get.'outlet_id='.$outlet.'&';
	    }
	    if (isset($_GET['end_date'])) {
	        $end_date = $_GET['end_date'];
	        $get=$get.'end_date='.$end_date.'&';
	    }
		?>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="error-message">
	                 <?php $this->core_helper->showFlashAlert(); ?>                    
	                 <?php echo validation_errors(); ?>
	               </div>
					<form action="<?=base_url()?>raw_outlet/search" method="get">
						<?php $outlet=''; ?>
						<?php if(isset($_GET['outlet_id'])){$outlet=$_GET['outlet_id'];} ?>
						<div class="row" style="margin-top: 10px;">
							<div class="col-md-2">
								<div class="form-group">
									<label><?php echo 'Pilih outlet'; ?></label>
									<select name="outlet_id" class="form-control">
										<?php foreach($list_outlet as $s){ ?>
											<option <?php if($s->id==$outlet){echo "selected";} ?> value=<?php echo $s->id ?>> <?php echo $s->name; ?> </option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label><?php echo "Dari tanggal" ?></label>
									<input type="date" value="<?php echo $start_date ?>" name="start_date" class="form-control" id="startDate" style="height: 35px" />
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label><?php echo "Sampai tanggal" ?></label>
									<input type="date" value="<?php echo $end_date ?>"  name="end_date" class="form-control" id="endDate" style="height: 35px" />
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label>&nbsp;</label><br />
									<input type="hidden" name="report" value="1" />
									<button class="btn btn-primary btn-block" style="; height: 35px;">&nbsp;&nbsp;<?php echo "Search"; ?>&nbsp;&nbsp;</button>
									&nbsp;&nbsp;
								</div>
							</div>
							<div class="col-md-2"><label>&nbsp;</label><br />
								<a href="<?php echo base_url('raw_outlet/history_excel'.$get) ?>" class="btn btn-success btn-block">Export excel</a></div>
						</div>
					</form>
					
					<div class="row" style="margin-top: 0px;">
						<div class="col-md-12">
							
						<div class="table-responsive">
							<?php if(isset($_GET['outlet_id']) && !empty($_GET['outlet_id'])){ ?>
								<table class="table">
								    <thead>
								    	<tr>
								    		<th width="16%"><?php echo 'Nama Outlet'; ?></th>
								    		<th width="16%"><?php echo 'Tanggal'; ?></th>
									    	<th width="16%"><?php echo 'Raw name'; ?></th>
									    	<th width="16%"><?php echo 'Total Stok'; ?></th>
	                                        <th width="16%"><?php echo 'Terpakai'; ?></th>
	                                        <th width="16%"><?php echo 'Sisa'; ?></th>
										</tr>
								    </thead>
									<tbody>
										<?php foreach($raw_outlet_list as $s){ ?>
											<tr>
												<th><?php echo $s->name; ?></th>
												<th><?php echo $s->i_date; ?></th>
												<td><?php echo $s->rm_name; ?></td>
												<td><?php echo $s->total_stock." ".$s->rm_unit; ?></td>
												<td><?php echo $s->used_stock." ".$s->rm_unit; ?></td>
												<td><?php echo ($s->total_stock-$s->used_stock)." ".$s->rm_unit; ?></td>
											</tr>
	                                    <?php } ?>
									</tbody>
								</table>
	                            <?php echo $this->pagination->create_links(); ?>
	                        <?php }else{ ?>
	                        	<div class="alert alert-info">Pilih outlet terlebih dahulu</div>
	                        <?php } ?>
						</div>
							
						</div>
					</div>
					
				</div><!-- Panel Body // END -->
			</div><!-- Panel Default // END -->
		</div><!-- Col md 12 // END -->
	</div><!-- Row // END -->
	
	<br /><br /><br />
	
</div><!-- Right Colmn // END -->
	
	
	
<?php
    require_once 'includes/footer.php';
?>