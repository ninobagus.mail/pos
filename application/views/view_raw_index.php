<?php
    require_once 'includes/header.php';
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script>
	$( function() {
		$( "#startDate" ).datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});
		
		$("#endDate").datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});
	} );
</script>

<!-- Add jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo "raw material - view" ?></h1>
		</div>
	</div><!--/.row-->

<?php
    $search_name = '';
    $search_code = '';
    if (isset($_GET['search_name'])) {
        $search_name = $_GET['search_name'];
    }
    if (isset($_GET['search_code'])) {
        $search_code = $_GET['search_code'];
    }
?>
	
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="error-message">
	                 <?php $this->core_helper->showFlashAlert(); ?>                    
	                 <?php echo validation_errors(); ?>
	               </div>
					<form action="<?=base_url()?>raw" method="get">
						<div class="row" style="margin-top: 10px;">
							<div class="col-md-3">
								<div class="form-group">
									<label><?php echo 'Name'; ?></label>
									<input type="text" name="search_name" class="form-control" style="height: 35px" value="<?php echo $search_name; ?>" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label><?php echo 'Code'; ?></label>
									<input type="text" name="search_code" class="form-control" id="startDate" style="height: 35px" value="<?php echo $search_code; ?>" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>&nbsp;</label><br />
									<input type="hidden" name="report" value="1" />
									<button class="btn btn-primary" style="width: 100%; height: 35px;">&nbsp;&nbsp;<?php echo "Search"; ?>&nbsp;&nbsp;</button>
								</div>
							</div>
						</div>
					</form>
					
					<div class="row" style="margin-top: 0px;">
						<div class="col-md-12">
							
						<div class="table-responsive">
							<table class="table" id="example">
							    <thead>
							    	<tr>
								    	<th width="16%"><?php echo 'Raw name'; ?></th>
								    	<th width="16%"><?php echo 'Raw Code'; ?></th>
                                        <th width="16%"><?php echo 'Raw unit'; ?></th>
									    <th width="16%"><?php echo 'Action'; ?></th>
									</tr>
							    </thead>
								<tbody>
								<?php foreach($raw_list as $r){ ?>
                                    <tr>
                                        <td><?php echo $r->rm_name; ?></td>
                                        <td><?php echo $r->rm_code; ?></td>
                                        <td><?php echo $r->rm_unit; ?></td>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <a href="<?php echo base_url('raw/edit/'.$r->rm_id) ?>" class="btn btn-info">Edit</a>
                                                </div>
                                                <div class="col-sm-6">
                                                    <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-edit-raw-<?php echo $r->rm_id ?>">Hapus</button>
                                                    <!-- Modal -->
                                                    <div id="modal-edit-raw-<?php echo $r->rm_id ?>" class="modal fade" role="dialog">
                                                      <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Hapus bahan mentah <b><?php echo $r->rm_name; ?></b></h4>
                                                          </div>
                                                          <div class="modal-body">
                                                            <form class="" method="POST" action="<?php echo base_url('raw/delete/'.$r->rm_id) ?>">
                                                                <div>
                                                                    <p>Anda yakin ingin menghapus data ini ? </p>
                                                                </div>
                                                                
                                                                <div>
                                                                    <button class="btn btn-warning" type="submit">Hapus</button>
                                                                    <button class="btn btn-deafult" data-dismiss="modal" type="submit">Batalkan</button>
                                                                </div>
                                                            </form>
                                                          </div>
                                                        </div>

                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>

                                    
								</tbody>
							</table>
                            <?php echo $this->pagination->create_links(); ?>
						</div>
							
						</div>
					</div>
					
				</div><!-- Panel Body // END -->
			</div><!-- Panel Default // END -->
		</div><!-- Col md 12 // END -->
	</div><!-- Row // END -->
	
	<br /><br /><br />
	
</div><!-- Right Colmn // END -->
	
	
	
<?php
    require_once 'includes/footer.php';
?>