<?php
    require_once 'includes/header.php';
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script>
	$( function() {
		$( "#trx_date" ).datepicker({
			format: "<?php echo $dateformat; ?>",
			autoclose: true
		});

	} );
</script>

<!-- Add jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url()?>assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo "raw material per supplier - tambah" ?></h1>
		</div>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					
					<div class="row" style="margin-top: 0px;">
						<div class="col-md-12">
						<div class="error-message">
		                 <?php $this->core_helper->showFlashAlert(); ?>                    
		                 <?php echo validation_errors(); ?>
		               </div>
							<form class="" method="POST" action="<?php echo base_url('raw_supplier/add_submit/') ?>">
								<div class="form-group">
									<p>Pilih supplier</p>
									<select class="form-control" name="supplier" required="">
										<?php foreach($list_supplier as $s){ ?>
											<option value="<?php echo $s->id ?>"><?php echo $s->name; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<p>Pilih Outlet</p>
									<select class="form-control" name="outlet" required="">
										<?php foreach($list_outlet as $s){ ?>
											<option value="<?php echo $s->id ?>"><?php echo $s->name. "( ".$s->address." )"; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<p>Pilih Bahan Mentah</p>
									<select class="form-control" name="raw" required="">
										<?php foreach($list_raw as $s){ ?>
											<option value="<?php echo $s->rm_id ?>"><?php echo $s->rm_name; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<p>Total supply</p>
									<input type="text" name="qty" class="form-control" required="">
								</div>
								<div class="form-group">
									<p>Tanggal Supply</p>
									<input type="date" name="trx_date" class="form-control" required="">
								</div>
								<div class="form-group">
									<p>Harga</p>
									<input type="text" name="price" class="form-control" required="">
								</div>
								<div class="form-group">
									<button class="btn btn-info" type="submit">Tambah</button>
								</div>
							</form>
						</div>
					</div>
					
				</div><!-- Panel Body // END -->
			</div><!-- Panel Default // END -->
		</div><!-- Col md 12 // END -->
	</div><!-- Row // END -->
	
	<br /><br /><br />
	
</div><!-- Right Colmn // END -->
	
	
	
<?php
    require_once 'includes/footer.php';
?>