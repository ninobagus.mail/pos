<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core_helper {
  private $_CI;
  public function __construct()
   {
		$this->_CI = & get_instance();
   }

  public function setFlashAlert($message=null , $type=null ,$isRedirect=false){
    $class='';
    $msg='';
    if($type=='SUCCESS'){
      $class='alert alert-success';
      $msg = $message;
    }
    else if($type=='FAIL')
    {
      $class='alert alert-danger';
      $msg = $message;
    }
    else if($type=='WARNING')
    {
      $class='alert alert-warning';
      $msg = $message;
    }
    else if($type=='INFO')
    {
      $class='alert alert-info';
      $msg = $message;
    }
    else{
      $class='alert alert-danger';
      $msg = $message;
    }

    $_SESSION['alert'] =  "<div class='".$class."'>".$msg." </div>";
    if($isRedirect){
      redirect($_SERVER['HTTP_REFERER']);
    }
  }
   
  public function showFlashAlert(){
    if(isset($_SESSION['alert'])){
      echo $_SESSION['alert'];
      unset($_SESSION['alert']);
    }
  }

  function convertToCurrency($amount){
      $currency = number_format($amount);
      //$currency= $currency + 50000;
      return "Rp. ".$currency;
    }

}

