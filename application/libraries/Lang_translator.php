<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lang_translator {
   
   public $data=array();
   public function __construct()
   {
   }

   public function get_translate()
   {
   	 	$this->CI =& get_instance();
        $data['lang_dashboard'] = $this->CI->lang->line('dashboard');
        $data['lang_customers'] = $this->CI->lang->line('customers');
        $data['lang_gift_card'] = $this->CI->lang->line('gift_card');
        $data['lang_add_gift_card'] = $this->CI->lang->line('add_gift_card');
        $data['lang_list_gift_card'] = $this->CI->lang->line('list_gift_card');
        $data['lang_debit'] = $this->CI->lang->line('debit');
        $data['lang_sales'] = $this->CI->lang->line('sales');
        $data['lang_today_sales'] = $this->CI->lang->line('today_sales');
        $data['lang_opened_bill'] = $this->CI->lang->line('opened_bill');
        $data['lang_reports'] = $this->CI->lang->line('reports');
        $data['lang_sales_report'] = $this->CI->lang->line('sales_report');
        $data['lang_expenses'] = $this->CI->lang->line('expenses');
        $data['lang_expenses_category'] = $this->CI->lang->line('expenses_category');
        $data['lang_pnl'] = $this->CI->lang->line('pnl');
        $data['lang_raw_material'] = $this->CI->lang->line('raw_material');
        $data['lang_raw_material_view']= $this->CI->lang->line('raw_material_view');
        $data['lang_raw_material_add']= $this->CI->lang->line('raw_material_add');
        $data['lang_pnl_report'] = $this->CI->lang->line('pnl_report');
        $data['lang_pos'] = $this->CI->lang->line('pos');
        $data['lang_return_order'] = $this->CI->lang->line('return_order');
        $data['lang_return_order_report'] = $this->CI->lang->line('return_order_report');
        $data['lang_inventory'] = $this->CI->lang->line('inventory');
        $data['lang_products'] = $this->CI->lang->line('products');
        $data['lang_list_products'] = $this->CI->lang->line('list_products');
        $data['lang_print_product_label'] = $this->CI->lang->line('print_product_label');
        $data['lang_product_category'] = $this->CI->lang->line('product_category');
        $data['lang_purchase_order'] = $this->CI->lang->line('purchase_order');
        $data['lang_setting'] = $this->CI->lang->line('setting');
        $data['lang_outlets'] = $this->CI->lang->line('outlets');
        $data['lang_users'] = $this->CI->lang->line('users');
        $data['lang_suppliers'] = $this->CI->lang->line('suppliers');
        $data['lang_system_setting'] = $this->CI->lang->line('system_setting');
        $data['lang_payment_methods'] = $this->CI->lang->line('payment_methods');
        $data['lang_logout'] = $this->CI->lang->line('logout');
        $data['lang_point_of_sales'] = $this->CI->lang->line('point_of_sales');
        $data['lang_amount'] = $this->CI->lang->line('amount');
        $data['lang_monthly_sales_outlet'] = $this->CI->lang->line('monthly_sales_outlet');
        $data['lang_create_return_order'] = $this->CI->lang->line('create_return_order');
        $data['lang_dashboard'] = $this->CI->lang->line('dashboard');
        $data['lang_customers'] = $this->CI->lang->line('customers');
        $data['lang_gift_card'] = $this->CI->lang->line('gift_card');
        $data['lang_add_gift_card'] = $this->CI->lang->line('add_gift_card');
        $data['lang_list_gift_card'] = $this->CI->lang->line('list_gift_card');
        $data['lang_debit'] = $this->CI->lang->line('debit');
        $data['lang_sales'] = $this->CI->lang->line('sales');
        $data['lang_today_sales'] = $this->CI->lang->line('today_sales');
        $data['lang_opened_bill'] = $this->CI->lang->line('opened_bill');
        $data['lang_reports'] = $this->CI->lang->line('reports');
        $data['lang_sales_report'] = $this->CI->lang->line('sales_report');
        $data['lang_expenses'] = $this->CI->lang->line('expenses');
        $data['lang_expenses_category'] = $this->CI->lang->line('expenses_category');
        $data['lang_pnl'] = $this->CI->lang->line('pnl');
        $data['lang_pnl_report'] = $this->CI->lang->line('pnl_report');
        $data['lang_pos'] = $this->CI->lang->line('pos');
        $data['lang_return_order'] = $this->CI->lang->line('return_order');
        $data['lang_return_order_report'] = $this->CI->lang->line('return_order_report');
        $data['lang_inventory'] = $this->CI->lang->line('inventory');
        $data['lang_products'] = $this->CI->lang->line('products');
        $data['lang_list_products'] = $this->CI->lang->line('list_products');
        $data['lang_print_product_label'] = $this->CI->lang->line('print_product_label');
        $data['lang_product_category'] = $this->CI->lang->line('product_category');
        $data['lang_purchase_order'] = $this->CI->lang->line('purchase_order');
        $data['lang_setting'] = $this->CI->lang->line('setting');
        $data['lang_outlets'] = $this->CI->lang->line('outlets');
        $data['lang_users'] = $this->CI->lang->line('users');
        $data['lang_suppliers'] = $this->CI->lang->line('suppliers');
        $data['lang_system_setting'] = $this->CI->lang->line('system_setting');
        $data['lang_payment_methods'] = $this->CI->lang->line('payment_methods');
        $data['lang_logout'] = $this->CI->lang->line('logout');
        $data['lang_point_of_sales'] = $this->CI->lang->line('point_of_sales');
        $data['lang_amount'] = $this->CI->lang->line('amount');
        $data['lang_monthly_sales_outlet'] = $this->CI->lang->line('monthly_sales_outlet');
        $data['lang_no_match_found'] = $this->CI->lang->line('no_match_found');
        $data['lang_create_return_order'] = $this->CI->lang->line('create_return_order');

        $data['lang_action'] = $this->CI->lang->line('action');
        $data['lang_edit'] = $this->CI->lang->line('edit');
        $data['lang_status'] = $this->CI->lang->line('status');
        $data['lang_add'] = $this->CI->lang->line('add');
        $data['lang_back'] = $this->CI->lang->line('back');
        $data['lang_update'] = $this->CI->lang->line('update');
        $data['lang_active'] = $this->CI->lang->line('active');
        $data['lang_inactive'] = $this->CI->lang->line('inactive');
        $data['lang_name'] = $this->CI->lang->line('name');
        $data['lang_search_product'] = $this->CI->lang->line('search_product');
        $data['lang_add_to_list'] = $this->CI->lang->line('add_to_list');
        $data['lang_submit'] = $this->CI->lang->line('submit');
        $data['lang_receive'] = $this->CI->lang->line('receive');
        $data['lang_view'] = $this->CI->lang->line('view');
        $data['lang_created'] = $this->CI->lang->line('created');
        $data['lang_tax'] = $this->CI->lang->line('tax');
        $data['lang_discount_amount'] = $this->CI->lang->line('discount_amount');
        $data['lang_total'] = $this->CI->lang->line('total');
        $data['lang_totat_payable'] = $this->CI->lang->line('totat_payable');
        $data['lang_discount'] = $this->CI->lang->line('discount');
        $data['lang_sale_id'] = $this->CI->lang->line('sale_id');
        $data['lang_tax_total'] = $this->CI->lang->line('tax_total');
        $data['lang_export_to_excel'] = $this->CI->lang->line('export_to_excel');
        $data['lang_type'] = $this->CI->lang->line('type');
        $data['lang_print'] = $this->CI->lang->line('print');

        $data['lang_customer_name'] = $this->CI->lang->line('customer_name');
        $data['lang_date_from'] = $this->CI->lang->line('date_from');
        $data['lang_date_to'] = $this->CI->lang->line('date_to');
        $data['lang_search'] = $this->CI->lang->line('search');
        $data['lang_sale_id'] = $this->CI->lang->line('sale_id');
        $data['lang_date'] = $this->CI->lang->line('date');
        $data['lang_grand_total'] = $this->CI->lang->line('grand_total');
        $data['lang_unpaid_amount'] = $this->CI->lang->line('unpaid_amount');
        $data['lang_make_payment'] = $this->CI->lang->line('make_payment');
        $data = array_unique($data);
        return($data);
   }
}